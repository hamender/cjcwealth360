import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './Home/index/index.component';
import { ProtectionComponent } from './Home/protection/protection.component';
import { ClientInfoComponent } from './Home/client-info/client-info.component';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AmountService } from './Home/services/amount.service';
import { StoreModule } from '@ngrx/store';
import { counterReducer } from './store/reducers/counter';
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    ProtectionComponent,
    ClientInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
   // NgbModule,
   HttpClientModule,
   StoreModule.forRoot({ 
    count: counterReducer
  })
  ],
  providers: [AmountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
