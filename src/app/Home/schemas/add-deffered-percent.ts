export class AddDefferedPercent {
  Index_value: number;
  Name: string;
  Start_age : string;
  Stop_age : string;
  percentage: string;
  Amount: string;
  Total_Amount: string;
}
