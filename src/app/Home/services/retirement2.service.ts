import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { TaxDeffered2 } from '../schemas/tax-deffered2';

@Injectable({
  providedIn: 'root'
})
export class Retirement2Service {
 //url = 'http://localhost:4000';
   url = 'http://cjcwealth360.com';
  constructor(private http:HttpClient) { }

  addAmount(param: TaxDeffered2):Observable<TaxDeffered2> {
	console.log(param)
		//return this.http.post<TaxDeffered2>(`${this.url}/deffered2/add`,param)
		return this.http.post<TaxDeffered2>(`${this.url}/api/deffered2/add`,param)
	}

	getamount(){
		//return this.http.get(`${this.url}/deffered2/`)
		return this.http.get(`${this.url}/api/deffered2/`)
	}
}
