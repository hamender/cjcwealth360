import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { TaxDefferd1 } from '../schemas/tax-defferd1';

@Injectable({
  providedIn: 'root'
})
export class Retirement1Service {
    //url = 'http://localhost:4000';
   url = 'http://cjcwealth360.com';
  constructor(private http:HttpClient) { }

  addAmount(param: TaxDefferd1):Observable<TaxDefferd1> {
	//console.log(param)
		//return this.http.post<TaxDefferd1>(`${this.url}/deffered1/add`,param)
		return this.http.post<TaxDefferd1>(`${this.url}/api/deffered1/add`,param)
	}

	getamount(){
		//return this.http.get(`${this.url}/deffered1/`)
		return this.http.get(`${this.url}/api/deffered1/`)
	}

	getCurrentA(parms){
		//return this.http.post(`${this.url}/deffered1/findOne/`,parms)
		return this.http.post(`${this.url}/api/deffered1/findOne/`,parms)
	}

	  addTax1(param: TaxDefferd1):Observable<TaxDefferd1> {
	console.log(param)
		//return this.http.post<TaxDefferd1>(`${this.url}/tax1/add`,param)
		return this.http.post<TaxDefferd1>(`${this.url}/api/tax1/add`,param)
	}

	getTax1(){
		//return this.http.get(`${this.url}/tax1/`)
		return this.http.get(`${this.url}/api/tax1/`)
	}

	  addDed1(param: TaxDefferd1):Observable<TaxDefferd1> {
	console.log(param)
		//return this.http.post<TaxDefferd1>(`${this.url}/ded1/add`,param)
		return this.http.post<TaxDefferd1>(`${this.url}/api/ded1/add`,param)
	}

	getDed1(){
		//return this.http.get(`${this.url}/ded1/`)
		return this.http.get(`${this.url}/api/ded1/`)
	}
}
