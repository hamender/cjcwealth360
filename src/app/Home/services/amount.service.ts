import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { Amountdetail } from '../schemas/amountdetail';
import { ColDetail } from '../schemas/col-detail';
import { Pension1 } from '../schemas/Pension1';
import { AddDefferedPercent } from '../schemas/add-deffered-percent';
@Injectable({
  providedIn: 'root'
})
export class AmountService {
   //url = 'http://localhost:4000';
   url = 'http://cjcwealth360.com';
  constructor(private http:HttpClient) { }

addAmount(param: Amountdetail):Observable<Amountdetail> {
	//console.log(param)
		//return this.http.post<Amountdetail>(`${this.url}/amount/add`,param)
		return this.http.post<Amountdetail>(`${this.url}/api/amount/add`,param)
	}

	getamount(){
		//return this.http.get(`${this.url}/amount/`)
		return this.http.get(`${this.url}/api/amount/`)
	}

colAmount(param: ColDetail):Observable<ColDetail>{
  //return this.http.post<ColDetail>(`${this.url}/collage/add`,param)
return this.http.post<ColDetail>(`${this.url}/api/collage/add`,param)

}

getcolamount(){
//return this.http.get(`${this.url}/collage/`)
		return this.http.get(`${this.url}/api/collage/`)
}

addpension(param: Pension1):Observable<Pension1> {
	//console.log(param)
		//return this.http.post<Pension1>(`${this.url}/pension1/add`,param)
		return this.http.post<Pension1>(`${this.url}/api/pension1/add`,param)
	}

	getpensionamount(){
		//return this.http.get(`${this.url}/pension1/`)
		return this.http.get(`${this.url}/api/pension1/`)
	}

	getCurramount(parms){
		//return this.http.post(`${this.url}/pension1/findOne/`,parms)
		return this.http.post(`${this.url}/api/pension1/findOne/`,parms)
	}

	addDeff1Percnt(param: AddDefferedPercent):Observable<AddDefferedPercent> {
	console.log(param)
		//return this.http.post<AddDefferedPercent>(`${this.url}/deff1_Percent/add`,param)
		return this.http.post<AddDefferedPercent>(`${this.url}/api/deff1_Percent/add`,param)
	}

	getDeff1Percnt(){
		//return this.http.get(`${this.url}/deff1_Percent/`)
		return this.http.get(`${this.url}/api/deff1_Percent/`)
	}

    }   
	  
