import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


    private messageSource = new BehaviorSubject('false');
  	currentMessage = this.messageSource.asObservable();

    constructor() {
    }

    changeMessage(message: string) {
    this.messageSource.next(message)
  }
}
