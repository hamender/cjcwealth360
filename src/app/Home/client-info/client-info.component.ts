import { Component, OnInit, ViewChild, ElementRef , Output, EventEmitter } from '@angular/core';
import * as myGlob from '../../globals';
import { IndexComponent } from '../index/index.component';
import { AuthService } from '../services/auth.service';
//import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-client-info',
  templateUrl: './client-info.component.html',
  styleUrls: ['./client-info.component.css']
})
export class ClientInfoComponent implements OnInit {
	
baseUrl= myGlob.baseUrl;
profileControls=1;
ChildDetails=1;
fundControl=1;
fundControl1=1;
show: boolean = false;
child: boolean = true;
child1: boolean = true;
concern: boolean = false;
Capital: boolean = false;
marked: boolean =false;
data: any;
message:string;
 
  theCheckbox = false;
  theCheckbox1 = false;
  theCheckbox2 = false;
  theCheckbox3 = false;
  theCheckbox4 = false;

  
  constructor(private myService: AuthService) { }
   name: any;
   parentPosts: any[]=[];


  ngOnInit(): void {
  }

  getdetails(event){
	let name = event.target.value;
	 this.myService.changeMessage(event.target.value);
}

  marrigeDetail(event){
  	if(event == 'yes'){
  		this.child = true;
  	}else{
  		this.child = false;
  	}
  }
  marrigeDetail1(event){
  	//console.log(event)
  	if(event == 'yes'){
  		this.child1 = true;
  	}else{
  		this.child1 = false;
  	}
  }

  funcAppendProfile(){
	 this.profileControls++;
 }

  funcRemoveProfile(){
	 if(this.profileControls>1)
		 this.profileControls--;  
 }

 ChildCol(){
 	this.ChildDetails++;

 }

 fundProfile(){
  this.fundControl++;
 }
  fundProfile1(){
  this.fundControl1++;
 }


 ChildExp(){
  if(this.ChildDetails>1)
  	this.ChildDetails--;
 }

 loopFromNumber(number){
 var items: number[] = [];
 for(var i = 1; i <= number; i++){
	 items.push(i);
 }
 return items;
}


showConcern(e){
  this.concern= e.target.checked;
}

showCaptial(e){
  this.Capital= e.target.checked;
}
  toggleVisibility(e){
   this.marked= e.target.checked;
}
showDetails(event){
    if(event == 'showplan'){
      this.show = true;
    }else{
      this.show = false;
    }

}
}
