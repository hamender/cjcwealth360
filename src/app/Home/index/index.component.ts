import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import * as myGlob from '../../globals'; //<==== this one
//import { environment } from '../../../environments/environment';
import { AuthService } from '../services/auth.service';
import { AmountService } from '../services/amount.service';
import { Retirement1Service } from '../services/retirement1.service';
import { Retirement2Service } from '../services/retirement2.service';

import { observable, Observable } from 'rxjs';
import { Amountdetail } from '../schemas/amountdetail';
//import { Store, select } from '@ngrx/store';
//import { Increment, Decrement, Reset } from '../../store/actions/counter';

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
	@ViewChild('txtAndrew') txtAndrew : ElementRef;
	@ViewChild('txtDebbie') txtDebbie : ElementRef;
	@ViewChild('RSAgAmnt') RSAgAmnt : ElementRef;
	@ViewChild('RSAActAmnt') RSAActAmnt : ElementRef;
	@ViewChild('txtPrf') txtPrf : ElementRef;
	@ViewChild('txtName') txtName : ElementRef;
    clickdef: boolean = false;
    beforeclick: boolean = true;
    showYear: boolean = true;
    TotalAllR: any = 0;
    TotalAllR1: any = 0;
    TotalAllR11: any = 0;
    TotalAll2: any = 0;
    startAge: any;
    indexValue: any;
    showPercent: any;
    stopAge: any;
    TotalAll1: any = 0;
      getPrev: any;
      showArr = [];
      showMiddleArrow: number = -1
      showMiddleArrowT: number = -1
      showMiddleArrowD: number = -1
      showMiddleArrowDef2: number = -1
     showName: any;
     showNamee: boolean = false;
    showAge: any;
    NamePension=[];

    NameControl=[];
    clientt=[];
    spousee=[];
    spouseName:any;
    clientName: any;
    accordianData: any;
    accordianData1: any;
    client: any;
    spouse: any
    start:any[]=[];
    stop:any[]=[];
    start1:any[]=[];
    stop1:any[]=[];
    iconShow :boolean = false;
    showdiff1: any;
    showdiff:any[]=[0] ;
    showData: any[]=[];
    expandedIndex = -1 
    globalName: any;
    mDef1: boolean = true;
    mDef11: boolean = false;
    mDef2: boolean = true;
    mDef22: boolean = false;
    mDef12: boolean = true;
    mTax: boolean = true;
    mDed: boolean = true;
    showdate: boolean = false;
	ShowtotalSaving: boolean = false;
	ShowtotalCollage:boolean = false;
	Showtotal: boolean = false;
	ShowMiddletotal:boolean = false;
	ShowMiddletotal1:boolean = false;
    ShowSideInvest: boolean = false;
	ShowMiddletotalTax:boolean = false;
	ShowMiddletotalDed:boolean = false;
	ShowMiddletotalTax1:boolean = false;
	ShowMiddletotalDed1:boolean = false;
    taxFMid1: boolean =true;
    shoeInvest2: boolean = true;
    ShowtotalIncome: boolean =false;
    showRetIncome: boolean = true;
    parms= {}
	ShowMidSaving: boolean = false;
	ShowMidCollage: boolean = false;
    ShowInvest: boolean = false;
    ShowInvestment: boolean =true;
	ShowSidetotal:boolean = false;
	ShowSideDeff2: boolean = false;
	ShowSideTax1: boolean = false;
	ShowSideDed1: boolean = false;
	ShowSideSav: boolean = false;
	ShowSideCol: boolean = false;
    showded2: boolean = true;
    ShowtotalRetired: boolean = false;
    ShowtotalRetTax: boolean = false;
    ShowtotalRetDed: boolean = false;
    ShowSided2: boolean = false;
	Showtotal1: boolean = false;
	Showtotal2: boolean = false;
	Showtotal3: boolean = false;
	Showtotal32: boolean = false;
    Showtotalt2: boolean = false;
	Showtotal4: boolean = false;
	Showtotal5: boolean = false;
	Showtotal6: boolean = false;
	Showtotal7: boolean = false;
	Showtotal8: boolean = false;
	ShowtotalInvest:boolean = false;
	ShowSideTax2: boolean = false;
	showTaxFree2: boolean = true;
	showRetTax2: boolean = true;
  	fetchAmt: any;
  	PlusAmt: any = 0;
  	InitialAge:any = 0;
  	ProjectedAge:any = 0;
  	showSideInvst: any = 0;
    showAmountIncome: any = 0;
	baseUrl= myGlob.baseUrl;
	intervalControls=1;
	intervalControl=1;
	profileControls=1;
	profileControl1=1;
    name: any;
	profilechildControls=1
	AndrewDisabControls=1;
	DebbieDisabControls=1;
	ProtecionLTControls=1
	ProtecionLTDebbieControls=1
	ProfileOccupationControl=3
	protectionLinsurControl=1
	pLinsurDebbControl=1
	savingControls=1
	savingControlscollage=1;
	//InvestIncmControls=1;
	RealEstateHomeControls=1;
	DebtWindowControls=1;
	//RealEstateCollageControls=1;
	RetirmentControls=1
	InvestControls=1;
	RetirmentControlsDeff=['Tax Deffered 1'];
	Retirment2ndControlsDeff=['Tax Deffered 1'];
	RetirmentTaxFreeDeff=['WD 1'];
	RetirmentTaxFreeDeff1=['WD 1'];

	RetirmentTaxFree2ndDeff=['WD 1'];
	RetirmentDeductDeff=['WD 1'];
	RetirmentTaxDeduct2ndDeff=['WD 1'];
	SavingControlsDeff=['SAVING 1'];
	CollageControlsDeff=['SAVING 1'];
    first: any;
    last: any;



	Retirment2ndControlsAmt:any[]=[0];
	Retirment2ndControlsA:any[]=[0];

	RetirmentTaxFree2ndAmt:any=[0];
	RetirmentTaxFree2ndA:any=[0];
	RetirmentTaxDeduct2ndA:any[]=[0];
    
	RetirmentTaxDeduct2ndAmt:any[]=[0];
	SavingControlsAmt:any[]=[0];
	SavingControlsA:any[]=[0];

	CollageControlsAmt:any[]=[0];
	CollageControlsA:any[]=[0];

    RetContTotCalAmt:any[]=[0];
    RetContTotCalAmt1:any[]=[0];

    RetContTotTax1:any[]=[0];
    RetContTotDed1:any[]=[0];
    RetContTotTax2:any[]=[0];
    RetContTotDed2:any[]=[0];

	SavAmount:any[]=[0];
	ColAmount:any[]=[0];
	TotSav:any[]=[0];
	TotCol:any[]=[0];
	InvestAmount: any[]=[0];
	rateInvst1: any[]=[0];
    
    RetirmentPension1: any[]=[0];
    RetirmentPension2: any[]=[0];
    RetPension1: any[]=[0];
    RetPension2: any[]=[0];

	InvesmentIncome:any[]=[0];
   	RetirmentTaxFreeAmt:any[]=[0];
   	RetirmentTaxFreeA:any[]=[0];
	RetirmentControlsDeffTax:any[]=[0];
	RetirmentControlsDeffTax1:any[]=[0];

	RetirmentControlsDeff1:any[]=[0];

	RetirmentControlsTaxFree:any[]=[0];
	RetirmentControlsDed:any[]=[0];
	RetirmentControlsDed1:any[]=[0];

	RetirmentControlsTaxFree1:any[]=[0];
    
	rateRet:any[]=[0];
	rateRet1:any[]=[0];
    rateTaxF1:any[]=[0];
    rateTaxF: any[]=[0];
    rateDed: any[]=[0];
    rateDed1: any[]=[0];

	InvestControlsDeff=['COMPANY NAME 1'];
	RetirmentControlsAmt:any[]=[0];
	RetirmentControlsA: any[]=[0];
	RetirmentDeductAmt:any[]=[0];
	RetirmentDeductA:any[]=[0];

	InvestControlsA:any[]=[0];    
	InvestControlsAmt:any[]=[0];
	RSAgrowthPercnt:any[]=[0];
	RSAgrowthPercnt1:any[]=[0];

	RSAgrowthPercntTex1:any[]=[0];
	RSAgrowthPercntDed1:any[]=[0];
	RSAgrowthPercntTex2:any[]=[0];
	RSAgrowthPercntDed2:any[]=[0];
	RSAgrowthInvest:any[]=[0];
	InvestAmt:any[]=[0];


	SavingSidbar:any[]=[0];
	CollageSidebar:any[]=[0];
    showMidInvest: any=0;
    
	RetireTotal: any=0;
	ShowMiddle: any=0;
	ShowMiddle1: any=0;
    showDeduct2: any=0;
	ShowMiddleTax1: any=0;
    ShowMiddleDed1: any=0;
    ShowMiddleTax2: any=0;
    ShowMiddleDed2: any=0;
    ShowMiddleSav: any=0;
    ShowMiddleCol: any=0;

	showSide: any=0;
	showSide1: any=0;
	showSide2: any=0;
	showSide3: any=0;
	showTax2: any= 0;
    showADed: any = 0;
	showAmountRetired: any =0;
	showAmountRet : any =0;

	showAmountTax: any =0;
	showAmountDed: any =0;
    showATax: any =0;
	Retire1: any=0;
	RetireTax: any=0;
	Deduct:any=0;
  Retiretaxfree2:any=0;
  RetireDeduct2:any=0;
  RetireSaving:any=0;

	InvestTotal:any=0;
	RSATaxfreeControls=1;
	RSATaxDeductionControls=1;
	RSAPensionControls=1;
	RSA2taxdefControls=1;
	RSA2taxFreeControls=1;
	RSA2taxDeductControls=1;
	RSA2PensionControls=1;


	RSAgrouwthAmount:any[]=[0];
	RSAgrouwthAmo:any[]=[0];

	RSAgrouwthAmount1:any[]=[0];
	RSAgrouwthAmo1:any[]=[0];

    RSAgrouwthAm2:any[]=[0];

	RSAgrouwthTax: any[]=[0];
	RSAgrouwthT: any[]=[0];

    RSAgrouwthDed: any[]=[0];
    RSAgrouwthD: any[]=[0];

    RSAgrouwthTax1: any[]=[0];
    RSAgrouwthDed1: any[]=[0];
    showRet:boolean = true;
	showRetire:boolean=true;
	showRetire2:boolean=true;
	showRetired:boolean=true;
    Showtotded: boolean =false;
    showRetDed2: boolean=true;
	IncreasePersnt:boolean = true;
	showRetire1:boolean= true;
	showRetireTax1: boolean = true;
	showRetireDed1: boolean = true;
	showDivSaving:boolean=true;
	showDivSaving1:boolean=true;
	showDivCollage:boolean=true;
	showDivCollage1:boolean=true;
    showRetdef:boolean = true;
	showRsaTaxFree:boolean=true;
	showRsaTaxFree1:boolean=true;
    showRetd: boolean = true;
	showRsaDedFree:boolean=true;
	showRsapensionFree:boolean=true;
	showRetSavAcc:boolean=true;
	showRetSavAcc1:boolean=true;
	showRetSavAcc2:boolean=true;
	showmidded2: boolean = true;
	showRetSavAcc3:boolean=true;
	InvestAndrew:boolean=true;
	amuntSav: boolean = true;
	amuntSav1: boolean = false;
	amuntCol: boolean =true;
	amuntCol1: boolean=false;
	amntTest: boolean = true;
	pensionTst: boolean = true;
	amntTax: boolean = true;
    
	amntTest1: boolean = false;
	amuntTax: boolean = true;
	amuntTax1: boolean = false;
	amuntded: boolean = true;
	amuntded1: boolean = false;
	amntdeff: boolean = true;
	amntdeff1: boolean = false;
	amnttaxF: boolean = true;
	amnttaxF1: boolean = false;
	amuntdeduct: boolean = true;
	amuntdeduct1: boolean = false;
	amntInvst: boolean = true;
	amntInvst1: boolean = false;
	aa:any='';
  CollageTotal:any;
  //style:number;
  data11:number;
  data: any;
  	message:string;
  	  width: number = 64;
  	   totalCount: number = 0;
	constructor(private myService: AuthService,
		private _amunt: AmountService,
		private _ret: Retirement1Service,
		private _retire: Retirement2Service,
		 private elem: ElementRef,
		) {

        this.myService.currentMessage.subscribe(message => {
        	if(message == 'false'){
        		return false;
        	}else{

        	this.globalName = message;
        	}
        })
    }

	ngOnInit() {
		//localStorage.setItem('key','hello')

	}

	funcAppendProfile(){
	 this.profileControls++;
 }
 funcRemoveProfile(){
	 if(this.profileControls>1)
		 this.profileControls--;  
 }
 funcAppendchildProfile(){
	 this.profilechildControls++;
 }
 funcRemovechildProfile(){
	 if(this.profilechildControls>1)
		 this.profilechildControls--;  
 }
 ProfileOccupationAdd(){
	this.ProfileOccupationControl++;
}
ProfileOccupationRemove(){
	if(this.ProfileOccupationControl>1)
		this.ProfileOccupationControl--;
} 

funcAddDisability(){
 this.AndrewDisabControls++;
}

funcRemoveDisability(){
	if(this.AndrewDisabControls>1)
		this.AndrewDisabControls--;  
}
funcAddDisabilityDebbie(){
	this.DebbieDisabControls++;
}
funcRemDisabilityDebbie(){
	if(this.DebbieDisabControls>1)
		this.DebbieDisabControls--;
}
ProtectionLTCareDebbieAdd(){
	this.ProtecionLTDebbieControls++;
}
ProtectionLTCareDebbieRem(){
	if(this.ProtecionLTDebbieControls>1)
		this.ProtecionLTDebbieControls--;
}

ProtectionLTCareAdd(){
	this.ProtecionLTControls++;
}
ProtectionLTCareRemove(){
	if(this.ProtecionLTControls>1)
		this.ProtecionLTControls--; 
}
ProptectionLifeInsurAdd(){
	this.protectionLinsurControl++;
}
ProptectionLifeInsurRemove(){
	if(this.protectionLinsurControl>1)
		this.protectionLinsurControl--;
}
ProtectionLInsurDebAdd(){
	this.pLinsurDebbControl++;
}
ProtectionLInsurDebRemove(){
	if(this.pLinsurDebbControl>1)
		this.pLinsurDebbControl--;
}

/*-----------------Saving Start Here---------------------*/

savingAddRow(){
	++this.totalCount;
	this.savingControls++;
	this.SavingControlsDeff.push('SAVING '+this.savingControls);
 this.SavingControlsAmt.push(0);
 this.SavingControlsA.push(0);

 this.SavAmount.push(0);
 this.SavingSidbar.push(0);
 this.TotSav.push(0);
 //this.style.width = ((this.value.length + 1) * 8) + 'px';
//console.log(this.style.width)


}
savingRemRow(){
	if(this.savingControls>1)
		this.savingControls--;
	 this.SavingControlsDeff.splice(this.savingControls,1);
	 this.SavingControlsAmt.splice(this.savingControls,1);
	 this.SavingControlsA.splice(this.savingControls,1);

	 this.SavAmount.splice(this.savingControls,1);
	 this.SavingSidbar.splice(this.savingControls,1);
	 this.TotSav.splice(this.savingControls,1);



}

collapseSaving(){
//console.log(this.data)
	this.showDivSaving=false;
	this.showDivSaving1=false;

	this.ShowtotalSaving = true;
	let total:number=0;
	for(var i in this.SavingControlsAmt) {	

		let fetchAmt1:any='';
		fetchAmt1=this.SavingControlsAmt[i];
		if(fetchAmt1!=0)

			total += parseInt(fetchAmt1.replace(/,/g, ''));
	

}
	this.RetireSaving = new Intl.NumberFormat().format(total);

	 let total1 :number=0;
	for(var i in this.SavAmount) {
		let fetch12: any = '';
		fetch12 = this.SavAmount[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddleSav = new Intl.NumberFormat().format(total1);
	this.ShowMidSaving = true;

	let total2 :number = 0;
	for(var i in this.TotSav){
		let fetch13:any = '';
		fetch13 = this.TotSav[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showSide1 = new Intl.NumberFormat().format(total2);
	this.ShowSideSav = true;
}



expandSaving(){
	//let data11 = ((this.data.length + 1) * 8) + 'px'
	//console.log(data11)
	/*pusharr.push(this.style1)
	let pusharr=[]
 for (let i = 0; i < this.data.length; i++) {
	console.log(i)
         if(this.data[i]!=0)
     }
     this.style = pusharr;
	console.log(this.style)*/
	this.showDivSaving=true;
	this.showDivSaving1=true;

	this.ShowtotalSaving = false;
	for(var i in this.SavingControlsAmt) {
         if(this.SavingControlsAmt[i]!=0)
			 this.SavingControlsAmt[i]= new Intl.NumberFormat().format(parseInt(this.SavingControlsAmt[i].replace(/,/g, '')));
		 else
		 this.SavingControlsAmt[i]= 0;
		}

	this.ShowMidSaving = false;


		for(var i in this.SavAmount) {
         if(this.SavAmount[i]!=0)
		 this.SavAmount[i]= new Intl.NumberFormat().format(parseInt(this.SavAmount[i].replace(/,/g, '')));
		 else
		 this.SavAmount[i]= 0;
	 
	}

	this.ShowSideSav = false;

		for(var i in this.TotSav) {
         if(this.TotSav[i]!=0)
		 this.TotSav[i]= new Intl.NumberFormat().format(parseInt(this.TotSav[i].replace(/,/g, '')));
		 else
		 this.TotSav[i]= 0;
	 
	}
}



/*	RSaving(element, i){
		console.log(i)*/
	RSaving(element){
		//for(var i=0; i>=0; i++){
		//}
	let savingA = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = savingA; 
  
    this.data = savingA;
}
SavingAmnut(i){

	if(this.SavAmount[i] && this.SavingSidbar[i]){
	let savingA = parseInt(this.SavAmount[i].replace(/,/g, '')); 
    let SavingControlsAmt:any =0;

    if(this.SavingControlsAmt[i]!=0)
	SavingControlsAmt= parseInt(this.SavingControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.SavingSidbar[i];
  //let diff = this.stop[i] - this.start[i];
  let diff = this.showAge;
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
     if(SavingControlsAmt>=0 || savingA>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    SavingControlsAmt+=savingA;
	   	    SavingControlsAmt+=(SavingControlsAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	 SavingControlsAmt+=savingA;
	   	    SavingControlsAmt+=(SavingControlsAmt*enteredRate)/100;
	   }	  
	   this.TotSav[i]=new Intl.NumberFormat().format(parseInt(SavingControlsAmt));
     }    
    }
}
//}



/*---------------Saving End Here------------*/
/*-----------------Collage Start Here---------------------*/
savingCollageAddRow(){
	++this.totalCount;
	this.savingControlscollage++;
		this.CollageControlsDeff.push('SAVING '+this.savingControlscollage);
 this.CollageControlsAmt.push(0);
 this.CollageControlsA.push(0);

 this.ColAmount.push(0);
 this.CollageSidebar.push(0);
 this.TotCol.push(0);



}
savingCollageRemRow(){
	if(this.savingControlscollage>1)
		this.savingControlscollage--;
	this.CollageControlsDeff.splice(this.savingControlscollage,1);
	 this.CollageControlsAmt.splice(this.savingControlscollage,1);
	 this.CollageControlsA.splice(this.savingControlscollage,1);

	 this.ColAmount.splice(this.savingControlscollage,1);
	 this.CollageSidebar.splice(this.savingControlscollage,1);
	 this.TotCol.splice(this.savingControlscollage,1);



}

collapseCollage(){
	this.showDivCollage=false;
	this.showDivCollage1=false;

	this.ShowtotalCollage = true;
	let total:number=0;
	for(var i in this.CollageControlsAmt) {
		let fetchAmt:any='';
		fetchAmt=this.CollageControlsAmt[i];
		if(fetchAmt!=0)
			total += parseInt(fetchAmt.replace(/,/g, ''));
	}

	this.CollageTotal = new Intl.NumberFormat().format(total);

	 let total1 :number=0;
	for(var i in this.ColAmount) {
		let fetch12: any = '';
		fetch12 = this.ColAmount[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddleCol = new Intl.NumberFormat().format(total1);
	this.ShowMidCollage = true;

	 let total2 :number=0;
	for(var i in this.TotCol) {
		let fetch12: any = '';
		fetch12 = this.TotCol[i];

		if(fetch12!=0)
		total2 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.showSide2 = new Intl.NumberFormat().format(total2);
	this.ShowSideCol = true;

}
expandCollage(){
	this.showDivCollage=true;
	this.showDivCollage1=true;

	this.ShowtotalCollage = false;
	for(var i in this.CollageControlsAmt) {
         if(this.CollageControlsAmt[i]!=0)
		 this.CollageControlsAmt[i]= new Intl.NumberFormat().format(parseInt(this.CollageControlsAmt[i].replace(/,/g, '')));
		 else
		 this.CollageControlsAmt[i]= 0;
		}


		this.ShowMidCollage = false;

		for(var i in this.ColAmount) {
         if(this.ColAmount[i]!=0)
		 this.ColAmount[i]= new Intl.NumberFormat().format(parseInt(this.ColAmount[i].replace(/,/g, '')));
		 else
		 this.ColAmount[i]= 0;
	 
	}

	this.ShowSideCol = false;

		for(var i in this.TotCol) {
         if(this.TotCol[i]!=0)
		 this.TotCol[i]= new Intl.NumberFormat().format(parseInt(this.TotCol[i].replace(/,/g, '')));
		 else
		 this.TotCol[i]= 0;
	 
	}
	}
	RCollage(element){
	let collageA = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = collageA; 
  
}
CollageAmnut(i){

	if(this.ColAmount[i] && this.CollageSidebar[i]){
	let collageA = parseInt(this.ColAmount[i].replace(/,/g, '')); 
    let CollageControlsAmt:any =0;

    if(this.CollageControlsAmt[i]!=0)
	CollageControlsAmt= parseInt(this.CollageControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.CollageSidebar[i];
  //let diff = this.stop[i] - this.start[i];
  let diff = this.showAge;
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(CollageControlsAmt>=0 || collageA>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    CollageControlsAmt+=collageA;
	   	    CollageControlsAmt+=(CollageControlsAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	 CollageControlsAmt+=collageA;
	   	    CollageControlsAmt+=(CollageControlsAmt*enteredRate)/100;
	   }	  
	   this.TotCol[i]=new Intl.NumberFormat().format(parseInt(CollageControlsAmt));
     }    
    }
}
/*-----------------Collage End Here---------------------*/


	/*InvestIncAndrewAdd(){
		this.InvestIncmControls++;
	}
	InvestIncAndrewRem(){
		if(this.InvestIncmControls>1)
			this.InvestIncmControls--;
	}*/
	

	ResateHomeResAdd(){
		this.RealEstateHomeControls++;
		this.DebtWindowControls++;
	}
	ResateHomeResRem(){
		if(this.RealEstateHomeControls>1)
			this.RealEstateHomeControls--;
		if(this.DebtWindowControls>1)
			this.DebtWindowControls--;
	}


	DebtWindowAdd(){
		this.DebtWindowControls++;
	}
	DebtWindowRem(){
		if(this.DebtWindowControls>1)
			this.DebtWindowControls--;
	}
	/*ResateCollageAdd(){
		this.RealEstateCollageControls++;
	}
	ResateCollageRem(){
		if(this.RealEstateCollageControls>1)
		this.RealEstateCollageControls--;
}*/






RSAPensionAdd(){
	this.RSAPensionControls++;
 this.RetirmentPension1.push(0);
 this.RetirmentPension2.push(0);
 this.NamePension.push();


}
RSAPensionRem(){
	if(this.RSAPensionControls>1)
		this.RSAPensionControls--;
	 this.RetirmentPension1.splice(this.RSAPensionControls,1);
	 this.RetirmentPension2.splice(this.RSAPensionControls,1);
	 this.NamePension.splice(this.RSAPensionControls,1);
}

 RSAPension(element){
	let RSAgrouwthP = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrouwthP; 
  
}

RSA2PensionAdd(){
	this.RSA2PensionControls++;
 this.RetPension1.push(0);
 this.RetPension2.push(0);

}
RSA2PensionRem(){
	if(this.RSA2PensionControls>1)
		this.RSA2PensionControls--;
	 this.RetPension1.splice(this.RSA2PensionControls,1);
	 this.RetPension2.splice(this.RSA2PensionControls,1);

}

 RSAPension1(element){
	let RSAgrouwthP1 = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrouwthP1; 
  
}
/******** Collapse/Expand Functions Starts **********/




/*-----Tax Free 1 Start Here-----------*/
RSATaxfreeAdd(){
	++this.totalCount;
	this.RSATaxfreeControls++;
	this.RetirmentTaxFreeDeff.push('WD '+this.RSATaxfreeControls);
 this.RetirmentTaxFreeAmt.push(0);
 this.RetirmentTaxFreeA.push(0);
 this.RSAgrouwthTax.push(0);
 this.RSAgrouwthT.push(0);

 this.RetContTotTax1.push(0);
 this.RSAgrowthPercntTex1.push(0);
 this.rateTaxF.push(0);
 this.RetirmentControlsTaxFree.push(0);




}
RSATaxfreeRem(){
	if(this.RSATaxfreeControls>1)
		this.RSATaxfreeControls--;
	 this.RetirmentTaxFreeDeff.splice(this.RSATaxfreeControls,1);
	 this.RetirmentTaxFreeAmt.splice(this.RSATaxfreeControls,1);
	 this.RetirmentTaxFreeA.splice(this.RSATaxfreeControls,1);

	 this.RSAgrouwthTax.splice(this.RSATaxfreeControls,1);
	 this.RSAgrouwthT.splice(this.RSATaxfreeControls,1);

	 this.RetContTotTax1.splice(this.RSATaxfreeControls,1);
	 this.RSAgrowthPercntTex1.splice(this.RSATaxfreeControls,1);
	 this.rateTaxF.splice(this.RSATaxfreeControls,1);
	 this.RetirmentControlsTaxFree.splice(this.RSATaxfreeControls,1);




}

RSATaxFreeCol(){

	this.showRsaTaxFree=false;
	this.Showtotal1= true;
	let total:number=0;
	for(var i in this.RetirmentTaxFreeAmt) {
		let fetchfree:any='';
		fetchfree=this.RetirmentTaxFreeAmt[i];
		if(fetchfree!=0)
			total += parseInt(fetchfree.replace(/,/g, ''));
		}

	/*	 for(var i in this.RetirmentTaxFreeA) {
		let fetch1:any = '';
		fetch1=this.RetirmentTaxFreeA[i];
		console.log(fetch1)
		       if(fetch1!=0)
				total += parseInt(fetch1.replace(/,/g, ''));				
	}*/

	this.RetireTax = new Intl.NumberFormat().format(total);
   this.showRsaTaxFree1 = false;
    this.ShowMiddletotalTax = true
	let total1 :number=0;
	for(var i in this.RSAgrouwthTax) {
		let fetch12: any = '';
		fetch12 = this.RSAgrouwthTax[i];
		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddleTax1 = new Intl.NumberFormat().format(total1);

	let total2 :number = 0;
	for(var i in this.RetContTotTax1){
		let fetch13:any = '';
		fetch13 = this.RetContTotTax1[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showSide = new Intl.NumberFormat().format(total2);
	this.ShowSideTax1 = true;

	 let total2Ret :number = 0;
	for(var i in this.RetirmentControlsTaxFree){
		let fetch13:any = '';
		fetch13 = this.RetirmentControlsTaxFree[i];
		if(fetch13!=0)
		total2Ret  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showAmountTax = new Intl.NumberFormat().format(total2Ret);

   this.ShowtotalRetTax = true;
  

}
RSATaxFreeExp(){
	this.showRsaTaxFree= true;
	this.Showtotal1= false;
    this.amntTax = true;
	for(var i in this.RetirmentTaxFreeAmt) {
         if(this.RetirmentTaxFreeAmt[i]!=0)
		 this.RetirmentTaxFreeAmt[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentTaxFreeAmt[i].replace(/,/g, '')));
		 else
		 this.RetirmentTaxFreeAmt[i]= 0;
	 
	}

	for(var i in this.RetirmentTaxFreeA) {
         if(this.RetirmentTaxFreeA[i]!=0)
		 this.RetirmentTaxFreeA[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentTaxFreeA[i].replace(/,/g, '')));
		 else
		 this.RetirmentTaxFreeA[i]= 0;
	 
	}


	this.showRsaTaxFree1= true;

	this.ShowMiddletotalTax = false;
	for(var i in this.RSAgrouwthTax) {
         if(this.RSAgrouwthTax[i]!=0)
		 this.RSAgrouwthTax[i]= new Intl.NumberFormat().format(parseInt(this.RSAgrouwthTax[i].replace(/,/g, '')));
		 else
		 this.RSAgrouwthTax[i]= 0;
	 
	}
  this.ShowSideTax1 = false;
	  for(var i in this.RetContTotTax1) {
         if(this.RetContTotTax1[i]!=0)
		 this.RetContTotTax1[i]= new Intl.NumberFormat().format(parseInt(this.RetContTotTax1[i].replace(/,/g, '')));
		 else
		 this.RetContTotTax1[i]= 0;
	 
	}
  this.ShowtotalRetTax = false;

	   for(var i in this.RetirmentControlsTaxFree) {
         if(this.RetirmentControlsTaxFree[i]!=0)
		 this.RetirmentControlsTaxFree[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsTaxFree[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsTaxFree[i]= 0;
	 
	}
	}


	RSAAmountTax1(element){
	let RSAgrouwthTax = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrouwthTax; 
  
}
RSAAmountCalTax(i){

	if(this.RSAgrouwthTax[i] && this.RSAgrowthPercntTex1[i]){

	let RSAgrouwthTax = parseInt(this.RSAgrouwthTax[i].replace(/,/g, '')); 
    let RetirmentTaxFreeAmt:any =0;

    if(this.RetirmentTaxFreeAmt[i]!=0)
	RetirmentTaxFreeAmt= parseInt(this.RetirmentTaxFreeAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntTex1[i];
    let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
   // let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentTaxFreeAmt>=0 || RSAgrouwthTax>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxFreeAmt+=RSAgrouwthTax;
	   	    RetirmentTaxFreeAmt+=(RetirmentTaxFreeAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	 RetirmentTaxFreeAmt+=RSAgrouwthTax;
	   	    RetirmentTaxFreeAmt+=(RetirmentTaxFreeAmt*enteredRate)/100;
	   }	  
	   this.RetContTotTax1[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFreeAmt));
     }    
    }

}

/*------Tax Free 1 End Here------*/

/*------------Tax Deduct 1 Start Here------*/	

RSATaxDeductionAdd(){
	++this.totalCount;
	this.RSATaxDeductionControls++;
	this.RetirmentDeductDeff.push('WD '+this.RSATaxDeductionControls);
 this.RetirmentDeductAmt.push(0);
 this.RetirmentDeductA.push(0);

 this.RSAgrouwthDed.push(0);
 this.RSAgrouwthD.push(0);

 this.RetContTotDed1.push(0);
 this.RSAgrowthPercntDed1.push(0);
 this.rateDed.push(0);
 this.RetirmentControlsDed.push(0);
}
RSATaxDeductionRem(){
	if( this.RSATaxDeductionControls>1)
	 this.RSATaxDeductionControls--;
	 this.RetirmentDeductDeff.splice(this.RSATaxDeductionControls,1);
	 this.RetirmentDeductAmt.splice(this.RSATaxDeductionControls,1);
	 this.RetirmentDeductA.splice(this.RSATaxDeductionControls,1);

	 this.RSAgrouwthDed.splice(this.RSATaxDeductionControls,1);
	 this.RSAgrouwthD.splice(this.RSATaxDeductionControls,1);
	 this.RetContTotDed1.splice(this.RSATaxDeductionControls,1);
	 this.RSAgrowthPercntDed1.splice(this.RSATaxDeductionControls,1);
	 this.rateDed.splice(this.RSATaxDeductionControls,1);
	 this.RetirmentControlsDed.splice(this.RSATaxDeductionControls,1);
}

RSAAmountDed1(element){
	let RSAgrouwthDed = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrouwthDed; 
  
}

RSATaxDedCol(){
	this.showRsaDedFree= false;
	this.Showtotal2= true;
	let total:number=0;
	for(var i in this.RetirmentDeductAmt) {
		let fetchAmt:any='';
		fetchAmt=this.RetirmentDeductAmt[i];
		       if(fetchAmt!=0)

			total += parseInt(fetchAmt.replace(/,/g, ''));
	
	}
	this.Deduct = new Intl.NumberFormat().format(total);

	this.ShowMiddletotalDed = true;

	let total1 :number=0;
	for(var i in this.RSAgrouwthDed) {
		let fetch12: any = '';
		fetch12 = this.RSAgrouwthDed[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddleDed1 = new Intl.NumberFormat().format(total1);

	let total2 :number = 0;
	for(var i in this.RetContTotDed1){
		let fetch13:any = '';
		fetch13 = this.RetContTotDed1[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showSide = new Intl.NumberFormat().format(total2);
	this.ShowSideDed1 = true;

	let total2Ret :number = 0;
	for(var i in this.RetirmentControlsDed){
		let fetch13:any = '';
		fetch13 = this.RetirmentControlsDed[i];
		if(fetch13!=0)
		total2Ret  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showAmountDed = new Intl.NumberFormat().format(total2Ret);

   this.ShowtotalRetDed = true;

}
RSATaxDedExp(){
	this.showRsaDedFree= true;
	this.Showtotal2= false;
		for(var i in this.RetirmentDeductAmt) {
         if(this.RetirmentDeductAmt[i]!=0)
		 this.RetirmentDeductAmt[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentDeductAmt[i].replace(/,/g, '')));
		 else
		 this.RetirmentDeductAmt[i]= 0;
	 
	}

	this.ShowMiddletotalDed = false;

   for(var i in this.RSAgrouwthDed) {
         if(this.RSAgrouwthDed[i]!=0)
		 this.RSAgrouwthDed[i]= new Intl.NumberFormat().format(parseInt(this.RSAgrouwthDed[i].replace(/,/g, '')));
		 else
		 this.RSAgrouwthDed[i]= 0;
	 
	}

	  this.ShowSideDed1 = false;
	  for(var i in this.RetContTotDed1) {
         if(this.RetContTotDed1[i]!=0)
		 this.RetContTotDed1[i]= new Intl.NumberFormat().format(parseInt(this.RetContTotDed1[i].replace(/,/g, '')));
		 else
		 this.RetContTotDed1[i]= 0;
	 
	}
   	  this.ShowtotalRetDed = false;
	  for(var i in this.RetirmentControlsDed) {
         if(this.RetirmentControlsDed[i]!=0)
		 this.RetirmentControlsDed[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsDed[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsDed[i]= 0;
	 
	}
}

RSAAmountCalDed(i){


	if(this.RSAgrouwthDed[i] && this.RSAgrowthPercntDed1[i]){
	let RSAgrouwthDed = parseInt(this.RSAgrouwthDed[i].replace(/,/g, '')); 
    let RetirmentDeductAmt:any =0;

    if(this.RetirmentDeductAmt[i]!=0)
	RetirmentDeductAmt= parseInt(this.RetirmentDeductAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntDed1[i];
    let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentDeductAmt>=0 || RSAgrouwthDed>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    RetirmentDeductAmt+=RSAgrouwthDed;
	   	    RetirmentDeductAmt+=(RetirmentDeductAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	 RetirmentDeductAmt+=RSAgrouwthDed;
	   	    RetirmentDeductAmt+=(RetirmentDeductAmt*enteredRate)/100;
	   }	  
	   this.RetContTotDed1[i]=new Intl.NumberFormat().format(parseInt(RetirmentDeductAmt));
     }    
    }

}

/*---------------Tax Deduct 1 End Here---------*/

/*-----------Tax Deffered 2 Start Here-----------*/
RSA2taxDefAdd(){
	++this.totalCount;
	this.RSA2taxdefControls++;
	this.Retirment2ndControlsDeff.push('Tax Deffered '+this.RSA2taxdefControls);
 this.Retirment2ndControlsAmt.push(0);
 this.Retirment2ndControlsA.push(0);

 this.RSAgrouwthAmount1.push(0);
 this.RSAgrouwthAmo1.push(0);

 this.RSAgrowthPercnt1.push(0);
 this.rateRet1.push(0);
 this.RetirmentControlsDeff1.push(0);
 this.RetContTotCalAmt1.push(0);
 this.RSAgrouwthAm2.push(0);
}

RSA2taxDefRem(){
	if(this.RSA2taxdefControls>1)
		this.RSA2taxdefControls--;
	this.Retirment2ndControlsDeff.splice(this.RSA2taxdefControls,1);
	 this.Retirment2ndControlsAmt.splice(this.RSA2taxdefControls,1);
	 this.Retirment2ndControlsA.splice(this.RSA2taxdefControls,1);

	 this.RSAgrouwthAmount1.splice(this.RSA2taxdefControls,1);
	 this.RSAgrouwthAmo1.splice(this.RSA2taxdefControls,1);

	 this.RSAgrowthPercnt1.splice(this.RSA2taxdefControls,1);
	 this.rateRet1.splice(this.RSA2taxdefControls,1);
	 this.RetirmentControlsDeff1.splice(this.RSA2taxdefControls,1);	
	 this.RetContTotCalAmt1.splice(this.RSA2taxdefControls,1);	
     this.RSAgrouwthAm2.splice(this.RSA2taxdefControls,1);
}

RetSavAccCol(){
	this.showRetSavAcc= false;
	let total:number=0;
	for(var i in this.Retirment2ndControlsAmt) {
	let fetchAmt:any='';
		fetchAmt=this.Retirment2ndControlsAmt[i];
		if(fetchAmt!=0)
		total += parseInt(fetchAmt.replace(/,/g, ''));
	}

	this.Retire1 = new Intl.NumberFormat().format(total);
	this.Showtotal3 = true;
	this.showRetdef= false;

	 let total1 :number=0;
	for(var i in this.RSAgrouwthAmount1) {
		let fetch12: any = '';
		fetch12 = this.RSAgrouwthAmount1[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddle1 = new Intl.NumberFormat().format(total1);
	this.ShowMiddletotal1 = true;

	 let total2 :number=0;
	for(var i in this.RetContTotCalAmt1){
		let fetch13:any = '';
		fetch13 = this.RetContTotCalAmt1[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showSide = new Intl.NumberFormat().format(total2);
	this.ShowSideDeff2 = true;
	this.showRet = false;



   let total3Ret :number=0;
for(var i in this.RetirmentControlsDeffTax1){
		let fetch12:any = '';
		fetch12 = this.RetirmentControlsDeffTax1[i];
		if(fetch12!=0)
		total3Ret  += parseInt(fetch12.replace(/,/g, ''));
	}
	this.showAmountRet = new Intl.NumberFormat().format(total3Ret);

   this.Showtotal32 = true;
  this.showRetd =false;


}

RetSavAccExp(){
	this.showRetSavAcc = true;
	this.Showtotal3 = false;
	for(var i in this.Retirment2ndControlsAmt) {
        if(this.Retirment2ndControlsAmt[i]!=0)
		 this.Retirment2ndControlsAmt[i]= new Intl.NumberFormat().format(parseInt(this.Retirment2ndControlsAmt[i].replace(/,/g, '')));
		 else
		 this.Retirment2ndControlsAmt[i]= 0;
	}
	this.showRetdef = true;
   this.ShowMiddletotal1 = false;
	for(var i in this.RSAgrouwthAmount1) {
         if(this.RSAgrouwthAmount1[i]!=0)
		 this.RSAgrouwthAmount1[i]= new Intl.NumberFormat().format(parseInt(this.RSAgrouwthAmount1[i].replace(/,/g, '')));
		 else
		 this.RSAgrouwthAmount1[i]= 0;
	 
	}
this.ShowSideDeff2 = false;
	this.showRet = true;

	 for(var i in this.RetContTotCalAmt1) {
         if(this.RetContTotCalAmt1[i]!=0)
		 this.RetContTotCalAmt1[i]= new Intl.NumberFormat().format(parseInt(this.RetContTotCalAmt1[i].replace(/,/g, '')));
		 else
		 this.RetContTotCalAmt1[i]= 0;
	 
	}



this.Showtotal32 = false;
	this.showRetd = true;

	 for(var i in this.RetirmentControlsDeffTax1) {
         if(this.RetirmentControlsDeffTax1[i]!=0)
		 this.RetirmentControlsDeffTax1[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsDeffTax1[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsDeffTax1[i]= 0;
	 
	}
	
}

RSAAmount1(element){
	let RSAgrowthAmout1 = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrowthAmout1; 
  
}

RSAAmountCalComplete1(i){

	if(this.RSAgrouwthAmount1[i] && this.RSAgrowthPercnt1[i]){
	let RSAgrowthAmout1 = parseInt(this.RSAgrouwthAmount1[i].replace(/,/g, '')); 
    let Retirment2ndControlsAmt:any =0;

    if(this.Retirment2ndControlsAmt[i]!=0)
	Retirment2ndControlsAmt= parseInt(this.Retirment2ndControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercnt1[i];
    let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
   // let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(Retirment2ndControlsAmt>=0 || RSAgrowthAmout1>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    Retirment2ndControlsAmt+=RSAgrowthAmout1;
	   	    Retirment2ndControlsAmt+=(Retirment2ndControlsAmt*enteredRate)/100;
	   	    	
	   }}
	   else{
	   	 Retirment2ndControlsAmt+=RSAgrowthAmout1;
	   	    Retirment2ndControlsAmt+=(Retirment2ndControlsAmt*enteredRate)/100;
	   }	  
	   this.RetContTotCalAmt1[i]=new Intl.NumberFormat().format(parseInt(Retirment2ndControlsAmt));
     }    
    }
}


/*-----------Tax Deffered 2 End Here-----------*/
/*-----------Tax Free 2 Start Here-----------*/
RSA2TaxFreeAdd(){
	++this.totalCount;
	this.RSA2taxFreeControls++;
	this.RetirmentTaxFree2ndDeff.push('WD '+this.RSA2taxFreeControls);
 this.RetirmentTaxFree2ndAmt.push(0);
  this.RetirmentTaxFree2ndDeff.splice(this.RSA2taxFreeControls,1);
 this.RSAgrouwthTax1.push(0);
 this.RetContTotTax2.push(0);
 this.rateTaxF1.push(0);
 this.RetirmentControlsTaxFree1.push(0);




}
RSA2TaxFreeRem(){
	if(this.RSA2taxFreeControls>1)
		this.RSA2taxFreeControls--;
	 this.RetirmentTaxFree2ndDeff.splice(this.RSA2taxFreeControls,1);
	 this.RSAgrouwthTax1.splice(this.RSA2taxFreeControls,1);
	 this.RetContTotTax2.splice(this.RSA2taxFreeControls,1);
	 this.rateTaxF1.splice(this.RSA2taxFreeControls,1);
	 this.RetirmentControlsTaxFree1.splice(this.RSA2taxFreeControls,1);



}

RetSavAcc1Col(){
	this.showRetSavAcc1= false;
	this.Showtotal4 = true;
	let total:number=0;
	for(var i in this.RetirmentTaxFree2ndAmt) {
		let fetchAmt:any='';
		fetchAmt=this.RetirmentTaxFree2ndAmt[i];
		if(fetchAmt!=0)
		total += parseInt(fetchAmt.replace(/,/g, ''));
		
	}

	this.Retiretaxfree2 = new Intl.NumberFormat().format(total);

	 this.ShowMiddletotalTax1 = true
	 this.taxFMid1 = false;
	let total1 :number=0;
	for(var i in this.RSAgrouwthTax1) {
		let fetch12: any = '';
		fetch12 = this.RSAgrouwthTax1[i];
		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddleTax2 = new Intl.NumberFormat().format(total1);

	let total2 :number = 0;
	for(var i in this.RetContTotTax2){
		let fetch13:any = '';
		fetch13 = this.RetContTotTax2[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showTax2 = new Intl.NumberFormat().format(total2);
	this.ShowSideTax2 = true;
	this.showTaxFree2 = false;

	let total3 :number = 0;
	for(var i in this.RetirmentControlsTaxFree1){
		let fetch4:any = '';
		fetch4 = this.RetirmentControlsTaxFree1[i];
		if(fetch4!=0)
		total3  += parseInt(fetch4.replace(/,/g, ''));
	}
	this.showATax = new Intl.NumberFormat().format(total3);
	this.Showtotalt2 = true;
	this.showRetTax2 = false;

}
RetSavAcc1Exp(){
	this.showRetSavAcc1 = true;
	this.Showtotal4 = false;
	for(var i in this.RetirmentTaxFree2ndAmt) {
       if(this.RetirmentTaxFree2ndAmt[i]!=0)
		 this.RetirmentTaxFree2ndAmt[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentTaxFree2ndAmt[i].replace(/,/g, '')));
		 else
		 this.RetirmentTaxFree2ndAmt[i]= 0;
	}

	this.ShowMiddletotalTax1 = false;
	this.taxFMid1 = true;
	for(var i in this.RSAgrouwthTax1) {
         if(this.RSAgrouwthTax1[i]!=0)
		 this.RSAgrouwthTax1[i]= new Intl.NumberFormat().format(parseInt(this.RSAgrouwthTax1[i].replace(/,/g, '')));
		 else
		 this.RSAgrouwthTax1[i]= 0;
	 
	}

	  this.ShowSideTax2 = false;
	  this.showTaxFree2 = true;
	  for(var i in this.RetContTotTax2) {
         if(this.RetContTotTax2[i]!=0)
		 this.RetContTotTax2[i]= new Intl.NumberFormat().format(parseInt(this.RetContTotTax2[i].replace(/,/g, '')));
		 else
		 this.RetContTotTax2[i]= 0;
	 
	}

      this.Showtotalt2 = false;
	  this.showRetTax2 = true;
	  for(var i in this.RetirmentControlsTaxFree1) {
         if(this.RetirmentControlsTaxFree1[i]!=0)
		 this.RetirmentControlsTaxFree1[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsTaxFree1[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsTaxFree1[i]= 0;
	 
	}
    
}

RSAAmountTax2(element){
	let RSAgrouwthTax1 = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrouwthTax1; 
  
}

RSAAmountCalTax1(i){

	if(this.RSAgrouwthTax1[i] && this.RSAgrowthPercntTex2[i]){
	let RSAgrouwthTax1 = parseInt(this.RSAgrouwthTax1[i].replace(/,/g, '')); 
    let RetirmentTaxFree2ndAmt:any =0;

    if(this.RetirmentTaxFree2ndAmt[i]!=0)
	RetirmentTaxFree2ndAmt= parseInt(this.RetirmentTaxFree2ndAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntTex2[i];
    let diff = this.showAge;
 // let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentTaxFree2ndAmt>=0 || RSAgrouwthTax1>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxFree2ndAmt+=RSAgrouwthTax1;
	   	    RetirmentTaxFree2ndAmt+=(RetirmentTaxFree2ndAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	RetirmentTaxFree2ndAmt+=RSAgrouwthTax1;
	   	    RetirmentTaxFree2ndAmt+=(RetirmentTaxFree2ndAmt*enteredRate)/100;
	   }	  
	   this.RetContTotTax2[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFree2ndAmt));
     }    
    }

}


/*-----------Tax Free 2 End Here-----------*/
/*-----------Tax Deductible 2 Start Here-----------*/
RSA2TaxeductAdd(){
	++this.totalCount;
	this.RSA2taxDeductControls++;
	 this.RetirmentTaxDeduct2ndDeff.push('WD '+this.RSA2taxDeductControls);
 this.RetirmentTaxDeduct2ndAmt.push(0);
 this.RetirmentTaxDeduct2ndA.push(0);

 this.RSAgrouwthDed1.push(0);
 this.RSAgrowthPercntDed2.push(0);
 this.RetContTotDed2.push(0);
 this.rateDed1.push(0);
 this.RetirmentControlsDed1.push(0);
}

RSA2TaxeductRem(){
	if(this.RSA2taxDeductControls>1)
		this.RSA2taxDeductControls--;
	this.RetirmentTaxDeduct2ndDeff.splice(this.RSA2taxDeductControls,1);
	 this.RetirmentTaxDeduct2ndAmt.splice(this.RSA2taxDeductControls,1);
	 this.RetirmentTaxDeduct2ndA.splice(this.RSA2taxDeductControls,1);

	 this.RSAgrouwthDed1.splice(this.RSA2taxDeductControls,1);
	 this.RSAgrowthPercntDed2.splice(this.RSA2taxDeductControls,1);
	 this.RetContTotDed2.splice(this.RSA2taxDeductControls,1);
	 this.rateDed1.splice(this.RSA2taxDeductControls,1);
	 this.RetirmentControlsDed1.splice(this.RSA2taxDeductControls,1);
}

RetSavAcc2Col(){
	this.showRetSavAcc2= false;
	this.Showtotal5 = true;
	let total:number=0;
	for(var i in this.RetirmentTaxDeduct2ndAmt) {
			let fetchAmt:any='';
		fetchAmt=this.RetirmentTaxDeduct2ndAmt[i];
		if(fetchAmt!=0)

			total += parseInt(fetchAmt.replace(/,/g, ''));
		
	}

	this.RetireDeduct2 = new Intl.NumberFormat().format(total);

	this.ShowMiddletotalDed1 = true;
    this.showmidded2 = false;
	let total1 :number=0;
	for(var i in this.RSAgrouwthDed1) {
		let fetch12: any = '';
		fetch12 = this.RSAgrouwthDed1[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddleDed2 = new Intl.NumberFormat().format(total1);

	this.showded2 = false;
    this.ShowSided2 = true;
	let total2 :number=0;
	for(var i in this.RetContTotDed2) {
		let fetch1: any = '';
		fetch1 = this.RetContTotDed2[i];

		if(fetch1!=0)
		total2 += parseInt(fetch1.replace(/,/g, ''));
	}
	this.showDeduct2 = new Intl.NumberFormat().format(total2);

	this.showRetDed2 = false;
    this.Showtotded = true;
	let total22 :number=0;
	for(var i in this.RetirmentControlsDed1) {
		let fetch12: any = '';
		fetch12 = this.RetirmentControlsDed1[i];

		if(fetch12!=0)
		total22 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.showADed = new Intl.NumberFormat().format(total22);

}
RetSavAcc2Exp(){
	this.showRetSavAcc2 = true;
	this.Showtotal5 = false;
	for(var i in this.RetirmentTaxDeduct2ndAmt) {
		  if(this.RetirmentTaxDeduct2ndAmt[i]!=0)
		 this.RetirmentTaxDeduct2ndAmt[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentTaxDeduct2ndAmt[i].replace(/,/g, '')));
		 else
		 this.RetirmentTaxDeduct2ndAmt[i]= 0;
	}

	this.ShowMiddletotalDed1 = false;
     this.showmidded2 = true;
	  for(var i in this.RSAgrouwthDed1) {
         if(this.RSAgrouwthDed1[i]!=0)
		 this.RSAgrouwthDed1[i]= new Intl.NumberFormat().format(parseInt(this.RSAgrouwthDed1[i].replace(/,/g, '')));
		 else
		 this.RSAgrouwthDed1[i]= 0;
	 
	}


this.showded2 = true;
     this.ShowSided2 = false;
	  for(var i in this.RetContTotDed2) {
         if(this.RetContTotDed2[i]!=0)
		 this.RetContTotDed2[i]= new Intl.NumberFormat().format(parseInt(this.RetContTotDed2[i].replace(/,/g, '')));
		 else
		 this.RetContTotDed2[i]= 0;
	 
	}

	this.showRetDed2 = true;
     this.Showtotded = false;
	  for(var i in this.RetirmentControlsDed1) {
         if(this.RetirmentControlsDed1[i]!=0)
		 this.RetirmentControlsDed1[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsDed1[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsDed1[i]= 0;
	 
	}
}

RSAAmountDed2(element){
	let RSAgrouwthDed1 = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrouwthDed1; 
  
}

RSAAmountCalDed1(i){


	if(this.RSAgrouwthDed1[i] && this.RSAgrowthPercntDed2[i]){
	let RSAgrouwthDed1 = parseInt(this.RSAgrouwthDed1[i].replace(/,/g, '')); 
    let RetirmentTaxDeduct2ndAmt:any =0;

    if(this.RetirmentTaxDeduct2ndAmt[i]!=0)
	RetirmentTaxDeduct2ndAmt= parseInt(this.RetirmentTaxDeduct2ndAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntDed2[i];
    let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentTaxDeduct2ndAmt>=0 || RSAgrouwthDed1>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxDeduct2ndAmt+=RSAgrouwthDed1;
	   	    RetirmentTaxDeduct2ndAmt+=(RetirmentTaxDeduct2ndAmt*enteredRate)/100;
	   	    	
	   }}else{	 
	   	 RetirmentTaxDeduct2ndAmt+=RSAgrouwthDed1;
	   	    RetirmentTaxDeduct2ndAmt+=(RetirmentTaxDeduct2ndAmt*enteredRate)/100;
	   } 
	   this.RetContTotDed2[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxDeduct2ndAmt));
     }    
    }

}


/*-----------Tax Deductible 2 End Here-----------*/
/*----------------Investment Start here-----------*/
InvestAndrewAdd(){
	++this.totalCount;
	this.InvestControls++;
	this.InvestControlsDeff.push('COMPANY NAME '+this.InvestControls);
	this.InvestControlsAmt.push(0);
	this.InvestControlsA.push(0);

	this.InvestAmount.push(0);
	this.RSAgrowthInvest.push(0);
	this.rateInvst1.push(0);
	this.InvesmentIncome.push(0);
	this.InvestAmt.push(0);
    


}
InvestAndrewRem(){
	if(this.InvestControls>1){
		this.InvestControls--;
		this.InvestControlsDeff.splice(this.InvestControls,1);
		this.InvestControlsAmt.splice(this.InvestControls,1);
		this.InvestControlsA.splice(this.InvestControls,1);

		this.InvestAmount.splice(this.InvestControls,1);
		this.RSAgrowthInvest.splice(this.InvestControls,1);
		this.rateInvst1.splice(this.InvestControls,1);
		this.InvesmentIncome.splice(this.InvestControls,1);
		this.InvestAmt.splice(this.InvestControls,1);
        

	}
}
InvestAndrewCol(){
	this.InvestAndrew= false;
	this.ShowtotalInvest = true;
	let total:number=0;
	for(var i in this.InvestControlsAmt) {
	 
	    let fetchVal:any=''; 
			 fetchVal=this.InvestControlsAmt[i];
		if(fetchVal!=0)

			total += parseInt(fetchVal.replace(/,/g, ''));
	 
	}
	this.InvestTotal = new Intl.NumberFormat().format(total);
	let total1:number=0;

	for(var i in this.InvestAmount) {
		let fetch12: any = '';
		fetch12 = this.InvestAmount[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.showMidInvest = new Intl.NumberFormat().format(total1);
	this.ShowInvest = true;
	this.ShowInvestment = false;

	let total2 :number = 0;
	for(var i in this.InvestAmt){
		let fetch13:any = '';
		fetch13 = this.InvestAmt[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showSideInvst = new Intl.NumberFormat().format(total2);
	this.ShowSideInvest = true;
	this.shoeInvest2 = false;

	let total3 :number = 0;
	for(var i in this.InvesmentIncome){
		let fetch14:any = '';
		fetch14 = this.InvesmentIncome[i];
		if(fetch14!=0)
		total3  += parseInt(fetch14.replace(/,/g, ''));
	}
	this.showAmountIncome = new Intl.NumberFormat().format(total3);
	this.ShowtotalIncome = true;
	this.showRetIncome = false;
}

InvestAndrewExp(){
	this.InvestAndrew = true;
	this.ShowtotalInvest = false;
	for(var i in this.InvestControlsAmt) {
		 if(this.InvestControlsAmt[i]!=0)
		 this.InvestControlsAmt[i]= new Intl.NumberFormat().format(parseInt(this.InvestControlsAmt[i].replace(/,/g, '')));
		 else
		 this.InvestControlsAmt[i]= 0;
		  
	}

		this.ShowInvestment = true;
   this.ShowInvest = false;
	for(var i in this.InvestAmount) {
         if(this.InvestAmount[i]!=0)
		 this.InvestAmount[i]= new Intl.NumberFormat().format(parseInt(this.InvestAmount[i].replace(/,/g, '')));
		 else
		 this.InvestAmount[i]= 0;
	 
	}

			this.shoeInvest2 = true;
   this.ShowSideInvest = false;
	for(var i in this.InvestAmt) {
         if(this.InvestAmt[i]!=0)
		 this.InvestAmt[i]= new Intl.NumberFormat().format(parseInt(this.InvestAmt[i].replace(/,/g, '')));
		 else
		 this.InvestAmt[i]= 0;
	 
	}

			this.showRetIncome = true;
   this.ShowtotalIncome = false;
	for(var i in this.InvesmentIncome) {
         if(this.InvesmentIncome[i]!=0)
		 this.InvesmentIncome[i]= new Intl.NumberFormat().format(parseInt(this.InvesmentIncome[i].replace(/,/g, '')));
		 else
		 this.InvesmentIncome[i]= 0;
	 
	}
}

	Investmentt(element){
	let InvestAmount = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = InvestAmount; 
  
}

InvestmentAmunt(i){
	if(this.InvestAmount[i] && this.RSAgrowthInvest[i]){
	let RSAgrowthAmout1 = parseInt(this.InvestAmount[i].replace(/,/g, '')); 
    let InvestControlsAmt:any =0;

    if(this.InvestControlsAmt[i]!=0)
	InvestControlsAmt= parseInt(this.InvestControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthInvest[i];
    let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(InvestControlsAmt>=0 || RSAgrowthAmout1>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    InvestControlsAmt+=RSAgrowthAmout1;
	   	    InvestControlsAmt+=(InvestControlsAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	InvestControlsAmt+=RSAgrowthAmout1;
	   	    InvestControlsAmt+=(InvestControlsAmt*enteredRate)/100;
	   }	  
	   this.InvestAmt[i]=new Intl.NumberFormat().format(parseInt(InvestControlsAmt));
     }    
    }

}

/*---------------Investment End Here---------------*/



RSAPensionCol(){
	this.showRsapensionFree= false;
}
RSAPensionExp(){
	this.showRsapensionFree = true;
}



RetSavAcc3Col(){
	this.showRetSavAcc3= false;
	this.Showtotal6 = true;

}
RetSavAcc3Exp(){
	this.showRetSavAcc3 = true;
	this.Showtotal6 = false;

}




transformAmount(element){
    	let RSAtaxDefAmount = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
      element.target.value = RSAtaxDefAmount; 


  }

RSAAmount(element){
	/*console.log('2')
	console.log(element)*/
	let RSAgrowthAmout = new Intl.NumberFormat().format(parseInt(element.target.value.replace(/,/g, '')));  
    element.target.value = RSAgrowthAmout; 
  //console.log(RSAgrowthAmout)
}

RSAAmountCalComplete(i){
/*console.log(i)
console.log('sssss')*/
	if(this.RSAgrouwthAmount[i] && this.RSAgrowthPercnt[i]){
	let RSAgrowthAmout = parseInt(this.RSAgrouwthAmount[i].replace(/,/g, '')); 
    let RetirmentControlsAmt:any =0;

    if(this.RetirmentControlsAmt[i]!=0)
	RetirmentControlsAmt= parseInt(this.RetirmentControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercnt[i];
    let diff = this.showAge;
     //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentControlsAmt>=0 || RSAgrowthAmout>=0 || enteredRate>=0){
      if(diff){
	   for(var l=0;l<diff;l++){
	   	    RetirmentControlsAmt+=RSAgrowthAmout;
	   	    RetirmentControlsAmt+=(RetirmentControlsAmt*enteredRate)/100;
	   	    	
	   }}else{
	   	 RetirmentControlsAmt+=RSAgrowthAmout;
	   	    RetirmentControlsAmt+=(RetirmentControlsAmt*enteredRate)/100;
	   }	    
	   this.RetContTotCalAmt[i]=new Intl.NumberFormat().format(parseInt(RetirmentControlsAmt));
     }
    }
}


funcColRetirement(){
	this.showRetire=false;

	let total:number=0;
	for(var i in this.RetirmentControlsAmt) {
		let fetch11:any = '';
		fetch11=this.RetirmentControlsAmt[i];
		       if(fetch11!=0)
				total += parseInt(fetch11.replace(/,/g, ''));				
	}
    
    for(var i in this.RetirmentControlsA) {
		let fetch1:any = '';
		fetch1=this.RetirmentControlsA[i];
		console.log(fetch1)
		       if(fetch1!=0)
				total += parseInt(fetch1.replace(/,/g, ''));				
	}
    this.RetireTotal = new Intl.NumberFormat().format(total);
	this.Showtotal = true;
	this.showRetire1=false;

     let total1 :number=0;
	for(var i in this.RSAgrouwthAmount) {
		let fetch12: any = '';
		fetch12 = this.RSAgrouwthAmount[i];

		if(fetch12!=0)
		total1 += parseInt(fetch12.replace(/,/g, ''));
	}
	this.ShowMiddle = new Intl.NumberFormat().format(total1);
	this.ShowMiddletotal = true;

	this.showRetire2 = false;

	let total2 :number = 0;
	for(var i in this.RetContTotCalAmt){
		let fetch13:any = '';
		fetch13 = this.RetContTotCalAmt[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showSide = new Intl.NumberFormat().format(total2);
	this.ShowSidetotal = true;
  
   let total2Ret :number = 0;
	for(var i in this.RetirmentControlsDeffTax){
		let fetch13:any = '';
		fetch13 = this.RetirmentControlsDeffTax[i];
		if(fetch13!=0)
		total2Ret  += parseInt(fetch13.replace(/,/g, ''));
	}
	this.showAmountRetired = new Intl.NumberFormat().format(total2Ret);

   this.ShowtotalRetired = true;
  this.showRetired =false;
}
funcExpRetirement(){

      this.showRetired =true;
	this.showRetire=true;
	this.showRetire2=true;
	//this.showRetire1=true;
	this.showRetire1=true;

	this.Showtotal = false;
	this.ShowMiddletotal = false;
	this.ShowSidetotal = false;
    this.ShowtotalRetired = false
         	this.amntTest = true;
	for(var i in this.RetirmentControlsAmt) {
         if(this.RetirmentControlsAmt[i]!=0){
		 this.RetirmentControlsAmt[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsAmt[i].replace(/,/g, '')));
		 console.log(this.RetirmentControlsAmt[i])
		 console.log(true)
		 }else{
		 this.RetirmentControlsAmt[i]= 0;
	 console.log(false)
	}}

		for(var i in this.RetirmentControlsA) {
         if(this.RetirmentControlsA[i]!=0)
		 this.RetirmentControlsA[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsA[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsA[i]= 0;
	 
	}

	for(var i in this.RSAgrouwthAmount) {
         if(this.RSAgrouwthAmount[i]!=0){
		 this.RSAgrouwthAmount[i]= new Intl.NumberFormat().format(parseInt(this.RSAgrouwthAmount[i].replace(/,/g, '')));
         	
		} else{
		 this.RSAgrouwthAmount[i]= 0;
		}
	 
	}

   for(var i in this.RetContTotCalAmt) {
         if(this.RetContTotCalAmt[i]!=0)
		 this.RetContTotCalAmt[i]= new Intl.NumberFormat().format(parseInt(this.RetContTotCalAmt[i].replace(/,/g, '')));
		 else
		 this.RetContTotCalAmt[i]= 0;
	 
	}

	   for(var i in this.RetirmentControlsDeffTax) {
         if(this.RetirmentControlsDeffTax[i]!=0)
		 this.RetirmentControlsDeffTax[i]= new Intl.NumberFormat().format(parseInt(this.RetirmentControlsDeffTax[i].replace(/,/g, '')));
		 else
		 this.RetirmentControlsDeffTax[i]= 0;
	 
	}


}

funcAddRetirement(){
	++this.totalCount;
 this.RetirmentControls++;
 this.rateRet.push(0);
 this.RetirmentControlsDeff.push('Tax Deffered '+this.RetirmentControls);
 this.RetirmentControlsAmt.push(0);
 this.RetirmentControlsA.push(0);
 this.RetirmentControlsDeffTax.push(0);
 this.RSAgrowthPercnt.push(0);
 this.RetContTotCalAmt.push(0); 
 this.RSAgrouwthAmount.push(0); 
 this.RSAgrouwthAmo.push(0); 

}
funcRemoveRetirement(){
	if(this.RetirmentControls>1){
	 this.RetirmentControls--;  
	 this.rateRet.splice(this.RetirmentControls,1);
	 this.RetirmentControlsDeff.splice(this.RetirmentControls,1);
	 this.RetirmentControlsDeffTax.splice(this.RetirmentControls,1);
	 this.RetirmentControlsAmt.splice(this.RetirmentControls,1);
	 this.RetirmentControlsA.splice(this.RetirmentControls,1);

	 this.RSAgrowthPercnt.splice(this.RetirmentControls,1)
	 this.RetContTotCalAmt.splice(this.RetirmentControls,1);
	 this.RSAgrouwthAmount.splice(this.RetirmentControls,1);
	 this.RSAgrouwthAmo.splice(this.RetirmentControls,1);


 }
}
/*********************************/

loopFromNumber(number){
 var items: number[] = [];
 for(var i = 1; i <= number; i++){
	 items.push(i);
 }
 return items;
}
 
getyear($evt,index){
	//let IntName = this.txtName[i].nativeElement.value;
	 //console.log(IntName)
	 this.first = this.start[index]
	 this.last = this.stop[index] || this.start[index+1]
this.showAge = this.stop[index] || this.start[index+1] - this.start[0];
this.showData[index] = this.stop[index] || this.start[index+1] - this.start[0];
this.showdiff1 = this.stop[index] || this.start[index+1] - this.start[index];
this.showdiff[index] = this.stop1[index] || this.start1[index+1]- this.start1[index];
this.showdiff[index] = this.stop[index] || this.start[index+1] - this.start[index];
/* let Agediff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;
   this.showdiff = Agediff;
   console.log(this.showdiff)
   this.showdate = true;
   this.InitialAge = this.txtAgePrf.nativeElement.value;
   this.ProjectedAge = this.txtAgePrjPrf.nativeElement.value;*/
}


retireLeft(){
     for(var index=0;index<=this.totalCount;index++){
	if(this.RetirmentControlsA[index]){
 	 let RetirmentControlsA = this.RetirmentControlsA[index].replace(/,/g, '');
 	 let rateRet = this.rateRet[index];
     let lastTax:any= (RetirmentControlsA*rateRet)/100;
      
     this.RetirmentControlsDeffTax[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
    }
  
    if(this.RetirmentTaxFreeA[index]){
 	 let RetirmentTaxFreeA = this.RetirmentTaxFreeA[index].replace(/,/g, '');
 	 let rateTaxF = this.rateTaxF[index];
     let lastTax:any= (RetirmentTaxFreeA*rateTaxF)/100;
      
      
     this.RetirmentControlsTaxFree[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
   } 

    if(this.RetirmentDeductA[index]){
 	 let RetirmentDeductA = this.RetirmentDeductA[index].replace(/,/g, '');
 	 let rateDed = this.rateDed[index];
     let lastTax:any= (RetirmentDeductA*rateDed)/100;
      
      
     this.RetirmentControlsDed[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
}
if(this.RetContTotCalAmt1[index]){
 	 let RetContTotCalAmt1 = this.RetContTotCalAmt1[index].replace(/,/g, '');
 	 let rateRet1 = this.rateRet1[index];
     let lastTax:any= (RetContTotCalAmt1*rateRet1)/100;
      
      
     this.RetirmentControlsDeffTax1[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
}
    if(this.RetContTotTax2[index]){
 	 let RetContTotTax2 = this.RetContTotTax2[index].replace(/,/g, '');
 	 let rateTaxF1 = this.rateTaxF1[index];
     let lastTax:any= (RetContTotTax2*rateTaxF1)/100;
       this.RetirmentControlsTaxFree1[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
}
    if(this.RetContTotDed2[index]){
 	 let RetContTotDed2 = this.RetContTotDed2[index].replace(/,/g, '');
 	 	 let rateDed1 = this.rateDed1[index];
     let lastTax:any= (RetContTotDed2*rateDed1)/100;
      
    this.RetirmentControlsDed1[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
}
   if(this.InvestAmt[index]){
 	 let InvestAmt = this.InvestAmt[index].replace(/,/g, '');
 	 let rateInvst1 = this.rateInvst1[index];
     let lastTax:any= (InvestAmt*rateInvst1)/100;
      
     this.InvesmentIncome[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
}
}
 }
 funcCalPrjAmt($evt,index){
 	 let enteredRate=$evt.target.value;
 	
if(this.RetirmentControlsAmt[index]){
 	 let RetirmentControlsAmt = this.RetirmentControlsAmt[index].replace(/,/g, '');
     let lastTax:any= (RetirmentControlsAmt*enteredRate)/100;/* this.RetirmentControlsAmt[index]= new Intl.NumberFormat().format(parseInt(enteredAmt));
      */
      
     this.RetirmentControlsDeffTax[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
    
  }


}
 funcCalPrjAmt1($evt,index){
 	 let enteredRate=$evt.target.value;

  if(this.RetirmentTaxFreeAmt[index]){
 	 let RetirmentTaxFreeAmt = this.RetirmentTaxFreeAmt[index].replace(/,/g, '');
     let lastTax:any= (RetirmentTaxFreeAmt*enteredRate)/100;
      
      
     this.RetirmentControlsTaxFree[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 

   } 
  }
 funcCalPrjAmt2($evt,index){
 	 let enteredRate=$evt.target.value;

   if(this.RetirmentDeductAmt[index]){
 	 let RetirmentDeductAmt = this.RetirmentDeductAmt[index].replace(/,/g, '');
     let lastTax:any= (RetirmentDeductAmt*enteredRate)/100;
      
      
     this.RetirmentControlsDed[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
}
    
  }

  funcCalPrj($evt,index){
  	 let enteredRate=$evt.target.value;

  if(this.RetContTotCalAmt1[index]){
 	 let RetContTotCalAmt1 = this.RetContTotCalAmt1[index].replace(/,/g, '');
     let lastTax:any= (RetContTotCalAmt1*enteredRate)/100;
      
      
     this.RetirmentControlsDeffTax1[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 

   } 

  }

  funcCalPrj1($evt,index){
  	 	 let enteredRate=$evt.target.value;
  	 	 if(this.RetContTotTax2[index]){
 	 let RetContTotTax2 = this.RetContTotTax2[index].replace(/,/g, '');
     let lastTax:any= (RetContTotTax2*enteredRate)/100;
      
      
     this.RetirmentControlsTaxFree1[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 

   } 
  }

    funcCalPrj2($evt,index){
  	 let enteredRate=$evt.target.value;

  if(this.RetContTotDed2[index]){
 	 let RetContTotDed2 = this.RetContTotDed2[index].replace(/,/g, '');
     let lastTax:any= (RetContTotDed2*enteredRate)/100;
      
      
     this.RetirmentControlsDed1[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 

   } 

  }

  funcCalInvest($evt,index){
 	 let enteredRate=$evt.target.value;
 	
if(this.InvestAmt[index]){
 	 let InvestAmt = this.InvestAmt[index].replace(/,/g, '');
     let lastTax:any= (InvestAmt*enteredRate)/100;
      
     this.InvesmentIncome[index]=new Intl.NumberFormat().format(parseInt(lastTax)); 
    
  }
 }


ShowName(i){
	this.showNamee = true;
	this.showName = this.NameControl[i];
}

AddAmount(i){
    let value = i;
 	this.indexValue = value;
 	this.name =  this.NameControl[i];
 	this.startAge = this.start[i];
 	this.stopAge = this.stop[i] || this.start[i+1];
 	this.showPercent = this.showdiff[i];
 	this.clientName = this.clientt[i];
 	this.spouseName = this.spousee[i];
 	let data = 50;
     for(var j=0;j<data;j++){
		if(this.SavingControlsAmt[j]){
	let savingA = parseInt(this.SavAmount[j].replace(/,/g, '')); 
    let SavingControlsAmt:any =0;

    if(this.SavingControlsAmt[j]!=0)
	SavingControlsAmt= parseInt(this.SavingControlsAmt[j].replace(/,/g, '')); 
    let enteredRate= this.SavingSidbar[j];
	         let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Client_Name': this.clientName,
  'Spouse_Name': this.spouseName,
  'Start_age' : this.startAge,
  'Stop_age' : this.stopAge,
  'Years' : this.showPercent,
  'MidAmount' : this.SavAmount[j],
  'Amountt' : this.SavingControlsAmt[j],
  'FinalAmount' : this.TotSav[j]
}	
  	this._amunt.addAmount(param).subscribe(res =>{
     console.log(res)
  	});

  	this._amunt.getamount().subscribe(res =>{
     console.log(res)
  	}); 
}}

 for(var j=0;j<data;j++){
		if(this.CollageControlsAmt[j]){
	let collageA = parseInt(this.ColAmount[j].replace(/,/g, '')); 
    let CollageControlsAmt:any =0;

    if(this.CollageControlsAmt[j]!=0)
	CollageControlsAmt= parseInt(this.CollageControlsAmt[j].replace(/,/g, '')); 
    let enteredRate= this.CollageSidebar[j];
	         let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Client_Name': this.clientName,
  'Spouse_Name': this.spouseName,
  'Start_age' : this.startAge,
  'Stop_age' : this.stopAge,
  'Years' : this.showPercent,
  'MidAmount' : this.ColAmount[j],
  'Amountt' : this.CollageControlsAmt[j],
  'FinalAmount' : this.TotCol[j]
}	
  	this._amunt.colAmount(param).subscribe(res =>{
     console.log(res)
  	});

  	this._amunt.getcolamount().subscribe(res =>{
     console.log(res)
  	}); 
}}


 for(var j=0;j<data;j++){
		if(this.RetirmentControlsAmt[j]){
	let RSAgrowthAmout = parseInt(this.RSAgrouwthAmount[j].replace(/,/g, '')); 
    let RetirmentControlsAmt:any =0;

    if(this.RetirmentControlsAmt[j]!=0)
	RetirmentControlsAmt= parseInt(this.RetirmentControlsAmt[j].replace(/,/g, '')); 
    let enteredRate= this.RSAgrowthPercnt[j];
	         let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Client_Name': this.clientName,
  'Spouse_Name': this.spouseName,
  'Start_age' : this.startAge,
  'Stop_age' : this.stopAge,
  'Years' : this.showPercent,
  'MidAmount' : this.RSAgrouwthAmount[j],
  'Amountt' : this.RetirmentControlsAmt[j],
  'FinalAmount' : this.RetContTotCalAmt[j],
  'Wd': this.rateRet[j],
  'Amount1': this.RetirmentControlsDeffTax[j],
}	
console.log(param)
  	this._ret.addAmount(param).subscribe(res =>{
     console.log(res)
  	}); 

  	this._ret.getamount().subscribe(res =>{
     console.log(res)
  	});
}}
  
     for(var j=0;j<data;j++){
		if(this.Retirment2ndControlsAmt[j]){
	let RSAgrowthAmout1 = parseInt(this.RSAgrouwthAmount1[j].replace(/,/g, '')); 
    let Retirment2ndControlsAmt:any =0;

    if(this.Retirment2ndControlsAmt[j]!=0)
	Retirment2ndControlsAmt= parseInt(this.Retirment2ndControlsAmt[j].replace(/,/g, '')); 
    let enteredRate= this.RSAgrowthPercnt1[j];
	         let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Client_Name': this.clientName,
  'Spouse_Name': this.spouseName,
  'Start_age' : this.startAge,
  'Stop_age' : this.stopAge,
  'Years' : this.showPercent,
  'MidAmount' : this.RSAgrouwthAmount1[j],
  'Amountt' : this.Retirment2ndControlsAmt[j],
  'FinalAmount' : this.RetContTotCalAmt1[j],
  'Wd': this.rateRet1[j],
  'Amount1': this.RetirmentControlsDeffTax1[j],
}	
  	this._retire.addAmount(param).subscribe(res =>{
     console.log(res)
  	});

  	this._retire.getamount().subscribe(res =>{
     console.log(res)
  	}); 
}}



     for(var j=0;j<data;j++){
		if(this.RetirmentTaxFreeAmt[j]){
	let RSAgrouwthTax = parseInt(this.RSAgrouwthTax[j].replace(/,/g, '')); 
    let RetirmentTaxFreeAmt:any =0;

    if(this.RetirmentTaxFreeAmt[j]!=0)
	RetirmentTaxFreeAmt= parseInt(this.RetirmentTaxFreeAmt[j].replace(/,/g, '')); 
    let enteredRate= this.RSAgrowthPercntTex1[j];
	         let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Client_Name': this.clientName,
  'Spouse_Name': this.spouseName,
  'Start_age' : this.startAge,
  'Stop_age' : this.stopAge,
  'Years' : this.showPercent,
  'MidAmount' : this.RSAgrouwthTax[j],
  'Amountt' : this.RetirmentTaxFreeAmt[j],
  'FinalAmount' : this.rateTaxF[j],
  'Wd': this.rateRet1[j],
  'Amount1': this.RetirmentControlsDeffTax1[j],
}	
  	this._ret.addTax1(param).subscribe(res =>{
     console.log(res)
  	});

  	this._ret.getTax1().subscribe(res =>{
     console.log(res)
  	}); 
}}


     for(var j=0;j<data;j++){
		if(this.RetirmentDeductAmt[j]){
	let RSAgrouwthDed = parseInt(this.RSAgrouwthDed[j].replace(/,/g, '')); 
    let RetirmentDeductAmt:any =0;

    if(this.RetirmentDeductAmt[j]!=0)
	RetirmentDeductAmt= parseInt(this.RetirmentDeductAmt[j].replace(/,/g, '')); 
    let enteredRate= this.RetirmentDeductAmt[j];
	         let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Client_Name': this.clientName,
  'Spouse_Name': this.spouseName,
  'Start_age' : this.startAge,
  'Stop_age' : this.stopAge,
  'Years' : this.showPercent,
  'MidAmount' : this.RSAgrouwthDed[j],
  'Amountt' : this.RetirmentDeductAmt[j],
  'FinalAmount' : this.RetContTotDed1[j],
  'Wd': this.rateDed[j],
  'Amount1': this.RetirmentControlsDeffTax1[j],
}	
  	this._ret.addDed1(param).subscribe(res =>{
     console.log(res)
  	});

  	this._ret.getDed1().subscribe(res =>{
     console.log(res)  
  	}); 
}}

     for(var j=0;j<data;j++){
     	if(this.RetirmentPension1[j]){
    let RetirmentPension1:any =0;
if(this.RetirmentPension1[j]!=0)
	RetirmentPension1= parseInt(this.RetirmentPension1[j].replace(/,/g, ''));
let param = {
  'Index_value': this.indexValue,
  'Name': this.name,
  'Start_age' : this.startAge,
  'Amountt' : this.RetirmentPension1[j],
}
console.log(param)
  	let parms = {
  		'name' : this.name
  	}

    this._amunt.getCurramount(parms).subscribe(res =>{
    	console.log(res)

        if(res == null){
        	console.log(true)
        	this._amunt.addpension(param).subscribe(res =>{
     console.log(res)
  	});

        }else{
        	console.log(false)
        }

  	}); 
}}
}
tstng(event, i){
if(event.target.value == 0 || event.target.value == null){
	this.amntTest = !this.amntTest;
}
}

Taxfree(event, i){
if(event.target.value == 0 || event.target.value == null){
	this.amntTax = !this.amntTax;
}
}

TaxDeduct(event, i){
if(event.target.value == 0 || event.target.value == null){
	this.amuntded = !this.amuntded;
}
}

tstDef(event, i){
	this.mDef12 = false
	if(event.target.value == 0 || event.target.value == null){
		this.showMiddleArrowDef2 = i;
	}

}

tsting(event, i){	
	if(this.RSAgrouwthAmount[i] == 0){
		this.mDef1 = false
	if(event.target.value == 0 || event.target.value == null){
		this.showMiddleArrow = i;
	}
}
}

taxMiddle(event, i){	
	if(this.RSAgrouwthTax[i] == 0){
		this.mTax = false
	if(event.target.value == 0 || event.target.value == null){
		this.showMiddleArrowT = i;
	}
}
}
deductMiddle(event, i){	
	if(this.RSAgrouwthDed[i] == 0){
		this.mDed = false
	if(event.target.value == 0 || event.target.value == null){
		this.showMiddleArrowD = i;
	}
}
}

PensionData(i){
	if(this.NameControl[0] == this.NameControl[i] && this.start[0] == this.start[i]){
	   	this.mDef1 = true;
	   	if(this.RSAgrouwthAmount[i]){
	   			let RSAgrowthAmout:any = parseInt(this.RSAgrouwthAmount[i].replace(/,/g, '')); 
	  this.RSAgrouwthAmount[i] = new Intl.NumberFormat().format(parseInt(RSAgrowthAmout));
	   	}
	   	  this.mTax = true;
	   	  	if(this.RSAgrouwthTax[i]){
	   			let RSAgrowthAmout1:any = parseInt(this.RSAgrouwthTax[i].replace(/,/g, '')); 
	  this.RSAgrouwthTax[i] = new Intl.NumberFormat().format(parseInt(RSAgrowthAmout1));
	   	}
	    this.mDed = true;
      	if(this.RSAgrouwthDed[i]){
	   			let RSAgrowthAmout2:any = parseInt(this.RSAgrouwthDed[i].replace(/,/g, '')); 
	  this.RSAgrouwthDed[i] = new Intl.NumberFormat().format(parseInt(RSAgrowthAmout2));
	   	}
 
           
	   }else{
	   	 this.mDef1 = false;
	   this.showMiddleArrow= -1
	   	  this.mTax = false;
	   this.showMiddleArrowT= -1
	    this.mDed = false;
	   this.showMiddleArrowD= -1
	   }
	    let value = i;
 	this.indexValue = value;
	 	this.name =  this.NameControl[i];
 	this.startAge = this.start[i];


  	let parms = {
  		'name' : this.name
  	}

    this._amunt.getCurramount(parms).subscribe(res =>{
    let data: any = res;
        if(data == null){
        this.RetirmentPension1[0] = 0;
        }else{
        	if(data.Name == this.name){
        		let amount = data.Amountt;
        		let totalamt = amount.replace(/,/g, '');
        		this.RetirmentPension1[0] = new Intl.NumberFormat().format(parseInt(totalamt)); 
        	}
        }
});
    this._ret.getCurrentA(parms).subscribe(res =>{
    	console.log(res)
    	let data: any = res;
        if(data == null){
        	console.log(false)
        //this.RetirmentPension1[0] = 0;
        }else{
        	console.log(true)
        	if(data.Name == this.name && data.Index_value == this.indexValue){
        		this.rateRet[0] = data.Wd; 
        	}
        }
    });

}

dataSavingPlus(ind=0){

     for(var i=0;i<=this.totalCount;i++){

     	/*----saving----*/

		if(this.SavingControlsAmt[i]){
	let savingA = parseInt(this.SavAmount[i].replace(/,/g, '')); 
    let SavingControlsAmt:any =0;
    let SavingControlsAmtP:any =0;
       if(this.SavingControlsAmt[i]!=0){
	SavingControlsAmt= parseInt(this.SavingControlsAmt[i].replace(/,/g, ''));
	SavingControlsAmtP= SavingControlsAmt;
}
    let enteredRate= this.SavingSidbar[i];
    let diff=0;
    let pDiff=0;
   for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     if(SavingControlsAmt>=0 || savingA>=0 || enteredRate>=0){
	   for(var l=0;l<diff;l++){
	   	    SavingControlsAmt+=savingA;
	   	    SavingControlsAmt+=(SavingControlsAmt*enteredRate)/100;	   	    	
	   }
	   for(var l=0;l<pDiff;l++){
	   	    SavingControlsAmtP+=savingA;
	   	    SavingControlsAmtP+=(SavingControlsAmtP*enteredRate)/100;	   	    	
	   }	  
	   this.amuntSav= false;
	   this.amuntSav1=true;
	   this.TotSav[i]=new Intl.NumberFormat().format(parseInt(SavingControlsAmt));
       this.SavingControlsA[i]= new Intl.NumberFormat().format(parseInt(SavingControlsAmtP));

      }   
}

/*----Collage-----*/
      if(this.CollageControlsAmt[i]){
	let collageA = parseInt(this.ColAmount[i].replace(/,/g, '')); 
    let CollageControlsAmt:any =0;
    let CollageControlsAmtP:any =0;

    if(this.CollageControlsAmt[i]!=0){
	CollageControlsAmt= parseInt(this.CollageControlsAmt[i].replace(/,/g, '')); 
  CollageControlsAmtP = CollageControlsAmt;
}
    let enteredRate=this.CollageSidebar[i];
  let diff=0;
    let pDiff=0;
   for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     
     if(CollageControlsAmt>=0 || collageA>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    CollageControlsAmt+=collageA;
	   	    CollageControlsAmt+=(CollageControlsAmt*enteredRate)/100;	   	    	
	   }
	   for(var l=0;l<pDiff;l++){
	   	    CollageControlsAmtP+=collageA;
	   	    CollageControlsAmtP+=(CollageControlsAmtP*enteredRate)/100;	   	    	
	   }
	    this.amuntCol= false;
	   this.amuntCol1=true;	  
	   this.TotCol[i]=new Intl.NumberFormat().format(parseInt(CollageControlsAmt));
this.CollageControlsA[i]= new Intl.NumberFormat().format(parseInt(CollageControlsAmtP));

     }    
   }
/*-----Tax Deffered 1---------*/
    if(this.RetirmentControlsAmt[i]){
	let RSAgrowthAmout = parseInt(this.RSAgrouwthAmount[i].replace(/,/g, ''));
    let RetirmentControlsAmt:any =0;
    let RetirmentControlsAmtP:any =0;
    if(this.RetirmentControlsAmt[i]!=0){
	RetirmentControlsAmt= parseInt(this.RetirmentControlsAmt[i].replace(/,/g, ''));
	RetirmentControlsAmtP= RetirmentControlsAmt;
    }
    let enteredRate=this.RSAgrowthPercnt[i];
    let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     if(RetirmentControlsAmt>=0 || RSAgrowthAmout>=0 || enteredRate>=0){
	   for(var l=0;l<diff;l++){
	   	    RetirmentControlsAmt+=RSAgrowthAmout;
	   	    RetirmentControlsAmt+=(RetirmentControlsAmt*enteredRate)/100;	   	    	
	   }
	   for(var l=0;l<pDiff;l++){
	   	    RetirmentControlsAmtP+=RSAgrowthAmout;
	   	    RetirmentControlsAmtP+=(RetirmentControlsAmtP*enteredRate)/100;	   	    	
	   }
       this.amntTest = false;
       this.amntTest1 = true;
       this.RetirmentControlsA[i]= new Intl.NumberFormat().format(parseInt(RetirmentControlsAmtP));
	   this.RetContTotCalAmt[i]=new Intl.NumberFormat().format(parseInt(RetirmentControlsAmt));
     }
    }

/*---Tax free1----*/

        if(this.RetirmentTaxFreeAmt[i]){
	let RSAgrouwthTax = parseInt(this.RSAgrouwthTax[i].replace(/,/g, '')); 
    let RetirmentTaxFreeAmt:any =0;
    let RetirmentTaxFreeAmtP:any =0;
    
    if(this.RetirmentTaxFreeAmt[i]!=0){
	RetirmentTaxFreeAmt= parseInt(this.RetirmentTaxFreeAmt[i].replace(/,/g, '')); 
    RetirmentTaxFreeAmtP= RetirmentTaxFreeAmt;
}
    let enteredRate=this.RSAgrowthPercntTex1[i];
     let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     if(RetirmentTaxFreeAmt>=0 || RSAgrouwthTax>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxFreeAmt+=RSAgrouwthTax;
	   	    RetirmentTaxFreeAmt+=(RetirmentTaxFreeAmt*enteredRate)/100;
	   	    	
	   }
	   for(var l=0;l<pDiff;l++){
	   	    RetirmentTaxFreeAmtP+=RSAgrouwthTax;
	   	    RetirmentTaxFreeAmtP+=(RetirmentTaxFreeAmtP*enteredRate)/100;	   	    	
	   }
	      
      this.amntTax = false;
       this.amuntTax1 = true;
       this.RetirmentTaxFreeA[i]= new Intl.NumberFormat().format(parseInt(RetirmentTaxFreeAmtP));
	   this.RetContTotTax1[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFreeAmt));
     }
  }
  /*---Tax Deductible----*/

  if(this.RetirmentDeductAmt[i]){
	let RSAgrouwthDed = parseInt(this.RSAgrouwthDed[i].replace(/,/g, '')); 
    let RetirmentDeductAmt:any =0;
    let RetirmentDeductAmtP:any =0;
    
    if(this.RetirmentDeductAmt[i]!=0){
	RetirmentDeductAmt= parseInt(this.RetirmentDeductAmt[i].replace(/,/g, ''));
	RetirmentDeductAmtP= RetirmentDeductAmt; 
}
    let enteredRate=this.RSAgrowthPercntDed1[i];
       let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
    
     if(RetirmentDeductAmt>=0 || RSAgrouwthDed>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentDeductAmt+=RSAgrouwthDed;
	   	    RetirmentDeductAmt+=(RetirmentDeductAmt*enteredRate)/100;	   	    	
	   }
	   for(var l=0;l<pDiff;l++){
	   	    RetirmentDeductAmtP+=RSAgrouwthDed;
	   	    RetirmentDeductAmtP+=(RetirmentDeductAmtP*enteredRate)/100;	   	    	
	   }
	   
	   this.amuntded = false;
       this.amuntded1 = true;	  
	   this.RetContTotDed1[i]=new Intl.NumberFormat().format(parseInt(RetirmentDeductAmt));
       this.RetirmentDeductA[i]= new Intl.NumberFormat().format(parseInt(RetirmentDeductAmtP));
     }    
    }

/*----Tax Deffered 2---*/
    if(this.Retirment2ndControlsAmt[i]){
	let RSAgrowthAmout1 = parseInt(this.RSAgrouwthAmount1[i].replace(/,/g, '')); 
    let Retirment2ndControlsAmt:any =0;
    let Retirment2ndControlsAmtP:any =0;
  
    if(this.Retirment2ndControlsAmt[i]!=0){
	Retirment2ndControlsAmt= parseInt(this.Retirment2ndControlsAmt[i].replace(/,/g, '')); 
    Retirment2ndControlsAmtP= Retirment2ndControlsAmt; 
}
    let enteredRate=this.RSAgrowthPercnt1[i];
        let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
    
     if(Retirment2ndControlsAmt>=0 || RSAgrowthAmout1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    Retirment2ndControlsAmt+=RSAgrowthAmout1;
	   	    Retirment2ndControlsAmt+=(Retirment2ndControlsAmt*enteredRate)/100;
	    }
	    for(var l=0;l<pDiff;l++){
	   	    Retirment2ndControlsAmtP+=RSAgrowthAmout1;
	   	    Retirment2ndControlsAmtP+=(Retirment2ndControlsAmtP*enteredRate)/100;
	    }	
	   this.mDef12 = false;
	   this.showMiddleArrowDef2= -1

	    this.amntdeff = false;
	    this.amntdeff1 = true;  
	   this.RetContTotCalAmt1[i]=new Intl.NumberFormat().format(parseInt(Retirment2ndControlsAmt));
       this.Retirment2ndControlsA[i]= new Intl.NumberFormat().format(parseInt(Retirment2ndControlsAmtP));
     
     }    
   }
/*-----Tax Free2--------*/  
 if(this.RetirmentTaxFree2ndAmt[i]){
	let RSAgrouwthTax1 = parseInt(this.RSAgrouwthTax1[i].replace(/,/g, '')); 
    let RetirmentTaxFree2ndAmt:any =0;
    let RetirmentTaxFree2ndAmtP:any =0;

    if(this.RetirmentTaxFree2ndAmt[i]!=0){
	RetirmentTaxFree2ndAmt= parseInt(this.RetirmentTaxFree2ndAmt[i].replace(/,/g, '')); 
    RetirmentTaxFree2ndAmtP = RetirmentTaxFree2ndAmt
}
    let enteredRate=this.RSAgrowthPercntTex2[i];
          let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     if(RetirmentTaxFree2ndAmt>=0 || RSAgrouwthTax1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxFree2ndAmt+=RSAgrouwthTax1;
	   	    RetirmentTaxFree2ndAmt+=(RetirmentTaxFree2ndAmt*enteredRate)/100;
	    }
	    	   for(var l=0;l<pDiff;l++){
	   	    RetirmentTaxFree2ndAmtP+=RSAgrouwthTax1;
	   	    RetirmentTaxFree2ndAmtP+=(RetirmentTaxFree2ndAmtP*enteredRate)/100;
	    }	
	    this.amnttaxF = false;
	    this.amnttaxF1 = true;  
	   this.RetContTotTax2[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFree2ndAmt));
	   this.RetirmentTaxFree2ndA[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFree2ndAmtP));
     
     }    
  } 
/*---Tax Deductible 2--------*/
   if(this.RetirmentTaxDeduct2ndAmt[i]){
	let RSAgrouwthDed1 = parseInt(this.RSAgrouwthDed1[i].replace(/,/g, '')); 
    let RetirmentTaxDeduct2ndAmt:any =0;
    let RetirmentTaxDeduct2ndAmtP:any =0;
    
    if(this.RetirmentTaxDeduct2ndAmt[i]!=0){
	RetirmentTaxDeduct2ndAmt= parseInt(this.RetirmentTaxDeduct2ndAmt[i].replace(/,/g, '')); 
    RetirmentTaxDeduct2ndAmtP = RetirmentTaxDeduct2ndAmt;
}
    let enteredRate=this.RSAgrowthPercntDed2[i];
     let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     if(RetirmentTaxDeduct2ndAmt>=0 || RSAgrouwthDed1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxDeduct2ndAmt+=RSAgrouwthDed1;
	   	    RetirmentTaxDeduct2ndAmt+=(RetirmentTaxDeduct2ndAmt*enteredRate)/100;
	   	    	
	   }	  
	    for(var l=0;l<pDiff;l++){
	   	    RetirmentTaxDeduct2ndAmtP+=RSAgrouwthDed1;
	   	    RetirmentTaxDeduct2ndAmtP+=(RetirmentTaxDeduct2ndAmtP*enteredRate)/100;
	   	    	
	   }
	   this.amuntdeduct = false;
	   this.amuntdeduct1 = true;
	   this.RetContTotDed2[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxDeduct2ndAmt));
	   this.RetirmentTaxDeduct2ndA[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxDeduct2ndAmtP));
    console.log(this.RetirmentTaxDeduct2ndA[i])
     }    
    }
/*------Investment--------*/
 if(this.InvestControlsAmt[i]){
	let RSAgrowthAmout1 = parseInt(this.InvestAmount[i].replace(/,/g, '')); 
    let InvestControlsAmt:any =0;
    let InvestControlsAmtP:any =0;
    
    if(this.InvestControlsAmt[i]!=0){
	InvestControlsAmt= parseInt(this.InvestControlsAmt[i].replace(/,/g, '')); 
    InvestControlsAmtP = InvestControlsAmt;
}
    let enteredRate=this.RSAgrowthInvest[i];
     let diff=0;
    let pDiff=0;
    for(var j=0;j<=ind;j++){
      diff += this.showdiff[j];
      if(j<ind)
      pDiff += this.showdiff[j];
    }
     if(InvestControlsAmt>=0 || RSAgrowthAmout1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    InvestControlsAmt+=RSAgrowthAmout1;
	   	    InvestControlsAmt+=(InvestControlsAmt*enteredRate)/100;
	   	}	
	   	for(var l=0;l<pDiff;l++){
	   	    InvestControlsAmtP+=RSAgrowthAmout1;
	   	    InvestControlsAmtP+=(InvestControlsAmtP*enteredRate)/100;
	   	} 
	   	this.amntInvst = false;
	   	this.amntInvst1 = true; 
	   this.InvestControlsA[i]=new Intl.NumberFormat().format(parseInt(InvestControlsAmtP));
	   this.InvestAmt[i]=new Intl.NumberFormat().format(parseInt(InvestControlsAmt));
     }    
    }


}}


 dataSaving(){
 	
 	let data = 50;
     for(var i=0;i<data;i++){
		if(this.SavingControlsAmt[i]){
	let savingA = parseInt(this.SavAmount[i].replace(/,/g, '')); 
    let SavingControlsAmt:any =0;

    if(this.SavingControlsAmt[i]!=0)
	SavingControlsAmt= parseInt(this.SavingControlsAmt[i].replace(/,/g, '')); 
    let enteredRate= this.SavingSidbar[i];
 //let diff = this.stop[i] - this.start[i];
 let diff = this.showAge;
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
     if(SavingControlsAmt>=0 || savingA>=0 || enteredRate>=0){
     	//this.SavingControlsAmt[i] = this.TotSav[i];
	   for(var l=0;l<diff;l++){
	   	    SavingControlsAmt+=savingA;
	   	    SavingControlsAmt+=(SavingControlsAmt*enteredRate)/100;
	   	    	
	   }	  
	   this.TotSav[i]=new Intl.NumberFormat().format(parseInt(SavingControlsAmt));

      }   
}
}
     for(var i=0;i<data;i++){

    if(this.CollageControlsAmt[i]){
	let collageA = parseInt(this.ColAmount[i].replace(/,/g, '')); 
    let CollageControlsAmt:any =0;

    if(this.CollageControlsAmt[i]!=0)
	CollageControlsAmt= parseInt(this.CollageControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.CollageSidebar[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(CollageControlsAmt>=0 || collageA>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    CollageControlsAmt+=collageA;
	   	    CollageControlsAmt+=(CollageControlsAmt*enteredRate)/100;
	   	    	
	   }	  
	   this.TotCol[i]=new Intl.NumberFormat().format(parseInt(CollageControlsAmt));
     }    
   }
}
  for(var i=0;i<data;i++){
    if(this.RetirmentControlsAmt[i]){
	let RSAgrowthAmout = parseInt(this.RSAgrouwthAmount[i].replace(/,/g, '')); 
    let RetirmentControlsAmt:any =0;

    if(this.RetirmentControlsAmt[i]!=0)
	RetirmentControlsAmt= parseInt(this.RetirmentControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercnt[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentControlsAmt>=0 || RSAgrowthAmout>=0 || enteredRate>=0){
	   for(var l=0;l<diff;l++){
	   	    RetirmentControlsAmt+=RSAgrowthAmout;
	   	    RetirmentControlsAmt+=(RetirmentControlsAmt*enteredRate)/100;	   	    	
	   }
      //this.changeAmount();
       this.amntTest = false;
       this.amntTest1 = true;
        this.RetirmentTaxFreeA[i]= this.RetContTotCalAmt[i];
	   this.RetContTotCalAmt[i]=new Intl.NumberFormat().format(parseInt(RetirmentControlsAmt));
     }    
    }
}
  for(var i=0;i<data;i++){
    if(this.RetirmentTaxFreeAmt[i]){
	let RSAgrouwthTax = parseInt(this.RSAgrouwthTax[i].replace(/,/g, '')); 
    let RetirmentTaxFreeAmt:any =0;

    if(this.RetirmentTaxFreeAmt[i]!=0)
	RetirmentTaxFreeAmt= parseInt(this.RetirmentTaxFreeAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntTex1[i];
     let diff = this.showAge;
 // let diff = this.stop[i] - this.start[i];
   // let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentTaxFreeAmt>=0 || RSAgrouwthTax>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxFreeAmt+=RSAgrouwthTax;
	   	    RetirmentTaxFreeAmt+=(RetirmentTaxFreeAmt*enteredRate)/100;
	   	    	
	   }
       this.amntTax = false;
	   	  this.RetirmentTaxFreeA[i] = this.RetContTotTax1[i];
	   this.RetContTotTax1[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFreeAmt));
    }    
    }
}
  for(var i=0;i<data;i++){
    if(this.RetirmentDeductAmt[i]){
	let RSAgrouwthDed = parseInt(this.RSAgrouwthDed[i].replace(/,/g, '')); 
    let RetirmentDeductAmt:any =0;

    if(this.RetirmentDeductAmt[i]!=0)
	RetirmentDeductAmt= parseInt(this.RetirmentDeductAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntDed1[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentDeductAmt>=0 || RSAgrouwthDed>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentDeductAmt+=RSAgrouwthDed;
	   	    RetirmentDeductAmt+=(RetirmentDeductAmt*enteredRate)/100;	   	    	
	   }	  
	   this.RetContTotDed1[i]=new Intl.NumberFormat().format(parseInt(RetirmentDeductAmt));
     }    
    }
}
  for(var i=0;i<data;i++){
    if(this.Retirment2ndControlsAmt[i]){
	let RSAgrowthAmout1 = parseInt(this.RSAgrouwthAmount1[i].replace(/,/g, '')); 
    let Retirment2ndControlsAmt:any =0;

    if(this.Retirment2ndControlsAmt[i]!=0)
	Retirment2ndControlsAmt= parseInt(this.Retirment2ndControlsAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercnt1[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
   // let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(Retirment2ndControlsAmt>=0 || RSAgrowthAmout1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    Retirment2ndControlsAmt+=RSAgrowthAmout1;
	   	    Retirment2ndControlsAmt+=(Retirment2ndControlsAmt*enteredRate)/100;
	   	    	
	   }	  
	   this.RetContTotCalAmt1[i]=new Intl.NumberFormat().format(parseInt(Retirment2ndControlsAmt));
     }    
   }
}
  for(var i=0;i<data;i++){
    if(this.RetirmentTaxFree2ndAmt[i]){
	let RSAgrouwthTax1 = parseInt(this.RSAgrouwthTax1[i].replace(/,/g, '')); 
    let RetirmentTaxFree2ndAmt:any =0;

    if(this.RetirmentTaxFree2ndAmt[i]!=0)
	RetirmentTaxFree2ndAmt= parseInt(this.RetirmentTaxFree2ndAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntTex2[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentTaxFree2ndAmt>=0 || RSAgrouwthTax1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxFree2ndAmt+=RSAgrouwthTax1;
	   	    RetirmentTaxFree2ndAmt+=(RetirmentTaxFree2ndAmt*enteredRate)/100;
	   	    	
	   }	  
	   this.RetContTotTax2[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxFree2ndAmt));
     }    
  }
    }
  for(var i=0;i<data;i++){
    if(this.RetirmentTaxDeduct2ndAmt[i]){
	let RSAgrouwthDed1 = parseInt(this.RSAgrouwthDed1[i].replace(/,/g, '')); 
    let RetirmentTaxDeduct2ndAmt:any =0;

    if(this.RetirmentTaxDeduct2ndAmt[i]!=0)
	RetirmentTaxDeduct2ndAmt= parseInt(this.RetirmentTaxDeduct2ndAmt[i].replace(/,/g, '')); 

    let enteredRate=this.RSAgrowthPercntDed2[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(RetirmentTaxDeduct2ndAmt>=0 || RSAgrouwthDed1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    RetirmentTaxDeduct2ndAmt+=RSAgrouwthDed1;
	   	    RetirmentTaxDeduct2ndAmt+=(RetirmentTaxDeduct2ndAmt*enteredRate)/100;
	   	    	
	   }	  
	   this.RetContTotDed2[i]=new Intl.NumberFormat().format(parseInt(RetirmentTaxDeduct2ndAmt));
     }    
    }
}
  for(var i=0;i<data;i++){
    if(this.InvestControlsAmt[i]){
	let RSAgrowthAmout1 = parseInt(this.InvestAmount[i].replace(/,/g, '')); 
    let InvestControlsAmt:any =0;

    if(this.InvestControlsAmt[i]!=0)
	InvestControlsAmt= parseInt(this.InvestControlsAmt[i].replace(/,/g, '')); 
    let enteredRate=this.RSAgrowthInvest[i];
     let diff = this.showAge;
  //let diff = this.stop[i] - this.start[i];
    //let diff=this.txtAgePrjPrf.nativeElement.value-this.txtAgePrf.nativeElement.value;	     
    
     if(InvestControlsAmt>=0 || RSAgrowthAmout1>=0 || enteredRate>=0){
      
	   for(var l=0;l<diff;l++){
	   	    InvestControlsAmt+=RSAgrowthAmout1;
	   	    InvestControlsAmt+=(InvestControlsAmt*enteredRate)/100;
	   	    	
	   }	  
	   this.InvestAmt[i]=new Intl.NumberFormat().format(parseInt(InvestControlsAmt));
     }    
    }
}
}


 /*---------------Interval Box Start Here---------------*/
InterValCol(){
 this.intervalControls++;
 //this.NameControl.push('Name '+this.intervalControls);
}

InterValRem(){
 if(this.intervalControls>1)
		 this.intervalControls--; 
		//this.NameControl.splice(this.intervalControls,1);
}

NameIntCol(i){
	this.accordianData = i;
	//console.log(this.accordianData)
this.showYear = false;
this.iconShow = true;
//this.expandedIndex = i === this.expandedIndex ? -1 : i;  
}

NameIntExp(i){
	this.accordianData = i;
	//console.log(i)
this.showYear = true;
this.iconShow = false;
//this.expandedIndex = i === this.expandedIndex ? -1 : i; 
//console.log(this.expandedIndex)
}
 /*---------------Interval Box End Here---------------*/



getname($evt){
	 this.client = this.txtAndrew.nativeElement.value;
}

getname1($evt){
	 this.spouse = this.txtDebbie.nativeElement.value;
}

intervaltest($evt,index){
	this.getyear($evt,index)
}

SumofAll(){
	let total2Ret :number = 0;
	for(var i in this.RetirmentPension1){
		let fetch13:any = '';
		fetch13 = this.RetirmentPension1[i];
		if(fetch13!=0)
		total2Ret  += parseInt(fetch13.replace(/,/g, ''));
	}
	let pension = total2Ret;
	

let total :number = 0;
	for(var i in this.RetirmentControlsDed){
		let fetch1:any = '';
		fetch1 = this.RetirmentControlsDed[i];
		if(fetch1!=0)
		total  += parseInt(fetch1.replace(/,/g, ''));
	}
	let deduct = total;


 let total4 :number = 0;
	for(var i in this.RetirmentControlsTaxFree){
		let fetch13:any = '';
		fetch13 = this.RetirmentControlsTaxFree[i];
		if(fetch13!=0)
		total4  += parseInt(fetch13.replace(/,/g, ''));
	}
	let taxfree = total4;
 let total2 :number = 0;
	for(var i in this.RetirmentControlsDeffTax){
		let fetch13:any = '';
		fetch13 = this.RetirmentControlsDeffTax[i];
		if(fetch13!=0)
		total2  += parseInt(fetch13.replace(/,/g, ''));
	}

	let taxDeffered = total2;

	if(pension >=0 || deduct >=0 || taxfree>=0 || taxDeffered>=0){
    pension+=deduct;
    taxfree+=pension;
    taxDeffered+=taxfree;
	}

	this.TotalAll1 = new Intl.NumberFormat().format(taxDeffered);
}

SumofAcc(){
	let total:number=0;
	for(var i in this.RetirmentControlsAmt) {
		let fetch11:any = '';
		fetch11=this.RetirmentControlsAmt[i];
		       if(fetch11!=0)
				total += parseInt(fetch11.replace(/,/g, ''));				
	}
	let defferd = total;

	let total1:number=0;
	for(var i in this.RetirmentTaxFreeAmt) {
		let fetchfree:any='';
		fetchfree=this.RetirmentTaxFreeAmt[i];
		if(fetchfree!=0)
			total1 += parseInt(fetchfree.replace(/,/g, ''));
	
	}
	let taxfree = total1;

		let total3:number=0;
	for(var i in this.RetirmentDeductAmt) {
		let fetchAmt:any='';
		fetchAmt=this.RetirmentDeductAmt[i];
		       if(fetchAmt!=0)

			total3 += parseInt(fetchAmt.replace(/,/g, ''));
	
	}
	let deduct = total3;
    

	if(defferd >=0 || deduct >=0 || taxfree>=0){
    defferd+=taxfree;
    deduct+=defferd;
    //deffered2+=deduct;
	}

	this.TotalAllR = new Intl.NumberFormat().format(deduct);
}

SumofAll2(){
		let total :number = 0;
	for(var i in this.RetPension1){
		let fetch13:any = '';
		fetch13 = this.RetPension1[i];
		if(fetch13!=0)
		total  += parseInt(fetch13.replace(/,/g, ''));
	}
	let pension = total;

   let total2 :number=0;
for(var i in this.RetirmentControlsDeffTax1){
		let fetch12:any = '';
		fetch12 = this.RetirmentControlsDeffTax1[i];
		if(fetch12!=0)
		total2  += parseInt(fetch12.replace(/,/g, ''));
	}
	let taxDeffered = total2;
let total3 :number = 0;
	for(var i in this.RetirmentControlsTaxFree1){
		let fetch4:any = '';
		fetch4 = this.RetirmentControlsTaxFree1[i];
		if(fetch4!=0)
		total3  += parseInt(fetch4.replace(/,/g, ''));
	}
	let taxfree = total3;
let total22 :number=0;
for(var i in this.RetirmentControlsDed1) {
		let fetch12: any = '';
		fetch12 = this.RetirmentControlsDed1[i];

		if(fetch12!=0)
		total22 += parseInt(fetch12.replace(/,/g, ''));
	}
	let deduct = total22;

	if(pension >=0 || deduct >=0 || taxfree>=0 || taxDeffered>=0){
    pension+=deduct;
    taxfree+=pension;
    taxDeffered+=taxfree;
	}

	this.TotalAll2 = new Intl.NumberFormat().format(taxDeffered);
}

SumofAcc1(){
		let total:number=0;
	for(var i in this.RetirmentTaxDeduct2ndAmt) {
			let fetchAmt:any='';
		fetchAmt=this.RetirmentTaxDeduct2ndAmt[i];
		if(fetchAmt!=0)

			total += parseInt(fetchAmt.replace(/,/g, ''));		
	}
	let deduct = total;

		let total1:number=0;
	for(var i in this.RetirmentTaxFree2ndAmt) {
		let fetch:any='';
		fetch=this.RetirmentTaxFree2ndAmt[i];
		if(fetch!=0)
		total1 += parseInt(fetch.replace(/,/g, ''));
		
	}
	let taxfree = total1;
   
   	let total3:number=0;
	for(var i in this.Retirment2ndControlsAmt) {
	let fetchA:any='';
		fetchA=this.Retirment2ndControlsAmt[i];
		if(fetchA!=0)
		total3 += parseInt(fetchA.replace(/,/g, ''));
	}
	let defferd = total3;


	if(defferd >=0 || deduct >=0 || taxfree>=0){
    defferd+=taxfree;
    deduct+=defferd;
	}

	this.TotalAllR1 = new Intl.NumberFormat().format(deduct);
}

Sumofclick(){
		let total4:number=0;
    for(var i in this.RetirmentControlsA) {
		let fetch12:any = '';
		fetch12=this.RetirmentControlsA[i];
		       if(fetch12!=0)
				total4 += parseInt(fetch12.replace(/,/g, ''));				
	}

	let deffered2 = total4;
	let total:number=0;
	for(var i in this.RetirmentTaxFreeA) {
		let fetchfree:any='';
		fetchfree=this.RetirmentTaxFreeA[i];
		if(fetchfree!=0)
			total += parseInt(fetchfree.replace(/,/g, ''));
	
	}
	let taxfree = total;

	let total3:number=0;
	for(var i in this.RetirmentDeductA) {
		let fetchf:any='';
		fetchf=this.RetirmentDeductA[i];
		if(fetchf!=0)
			total3 += parseInt(fetchf.replace(/,/g, ''));
	
	}
	let taxdedd = total3;
	if(deffered2 >=0 || taxfree>=0 || taxdedd>=0){
     deffered2+=taxfree;
     taxdedd+=deffered2;
	}
	this.TotalAllR11 = new Intl.NumberFormat().format(taxdedd);
this.clickdef = true;
this.beforeclick = false;
}


}