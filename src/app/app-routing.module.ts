import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './Home/index/index.component';
import { ProtectionComponent } from './Home/protection/protection.component';
import { ClientInfoComponent } from './Home/client-info/client-info.component';

const routes: Routes = [
    /*{
		path: 'dashboard',
		component: IndexComponent
	},
	{
		path: '',
		redirectTo: '/dashboard',
		pathMatch: 'full'
	},
	{
	   path: 'protection',
	   component: ProtectionComponent
	}*/

	{
  path: '',
  component: IndexComponent,
  children: [
  {
    path: 'dashboard',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  ]
},
{
  path: 'protection',
  component: ProtectionComponent,
},
{
	path : 'client-info',
	component: ClientInfoComponent,
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
