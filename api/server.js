const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    config = require('./DB');

   const amountRoute = require('./routes/amount.route');
   const colRoute = require('./routes/collage.route');
   const def1Route = require('./routes/deffered1.route');
   const def2Route = require('./routes/deffered2.route');
   const tax1Route = require('./routes/taxFree1.route');
   const ded1Route = require('./routes/taxDeductible1.route');
   const pensionRoute = require('./routes/pension1.route');
   const percent1Route = require('./routes/PercentDeff1.route');
   
   
    mongoose.Promise = global.Promise;
    mongoose.connect(config.DB, { useNewUrlParser: true }).then(
      () => {console.log('Database is connected') },
      err => { console.log('Can not connect to the database'+ err)}
    );     

    const app = express();
    app.use(bodyParser.json());
    app.use(cors());
    app.use('/amount', amountRoute);
    app.use('/collage', colRoute);
    app.use('/deffered1', def1Route);
    app.use('/deffered2', def2Route);
    app.use('/tax1', tax1Route);
    app.use('/ded1', ded1Route);
    app.use('/pension1', pensionRoute);
    app.use('/deff1_Percent', percent1Route);
    

    const port = process.env.PORT || 4000;

    const server = app.listen(port, function(){
     console.log('Listening on port ' + port);
    });