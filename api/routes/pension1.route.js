const express = require('express');
const app = express();
const pensionRoute = express.Router();

// Require Product model in our routes module
let Pension = require('../models/pension1');

// Defined store route
pensionRoute.route('/add').post(function (req, res) {
  let pension = new Pension(req.body);
  pension.save()
    .then(pension => {
      res.status(200).json({'Pension': 'Pension has been added successfully'});
    })     
    .catch(err => {
      console.log(err)
    res.status(400).send("unable to save to database");
    });
});



// Defined get data(index or listing) route
pensionRoute.route('/').get(function (req, res) {
  Pension.find(function (err, amount){
    if(err){
      console.log(err);
    }
    else {
      res.json(amount);
    }
  });
});

// Defined edit route
pensionRoute.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Pension.findById(id, function (err, amount){
      res.json(amount);
  });
});


pensionRoute.route('/findOne').post(function (req, res) {
  var Name = req.body.name;
  Pension.findOne({Name:Name}, function (err, amount){
      res.json(amount);

  });
});

//  Defined update route
/*pensionRoute.route('/update/:id').post(function (req, res) {
  Amount.findById(req.params.id, function(err, amount) {
    if (!amount)
      res.status(404).send("Record not found");
    else {
      amount.Index_value = req.body.Index_value;
      amount.Name = req.body.Name;
      amount.Start_age = req.body.Start_age;
      amount.Stop_age = req.body.Stop_age;
      amount.Years = req.body.Years;
      amount.MidAmount = req.body.MidAmount;
      amount.Amountt = req.body.Amountt;
      amount.FinalAmount = req.body.FinalAmount;
      

      amount.save().then(amount => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});*/

// Defined delete | remove | destroy route
/*pensionRoute.route('/delete/:id').get(function (req, res) {
    Amount.findByIdAndRemove({_id: req.params.id}, function(err, product){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});*/

module.exports = pensionRoute;