const express = require('express');
const app = express();
const colRoute = express.Router();

// Require Product model in our routes module
let Collage = require('../models/collage');

// Defined store route
colRoute.route('/add').post(function (req, res) {
  let collage = new Collage(req.body);
  collage.save()
    .then(collage => {
      res.status(200).json({'Collage': 'Collage has been added successfully'});
    })     
    .catch(err => {
      console.log(err)
    res.status(400).send("unable to save to database");
    });
});



// Defined get data(index or listing) route
colRoute.route('/').get(function (req, res) {
  Collage.find(function (err, collage){
    if(err){
      console.log(err);
    }
    else {
      res.json(collage);
    }
  });
});

// Defined edit route
colRoute.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Collage.findById(id, function (err, collage){
      res.json(collage);
  });
});

//  Defined update route
colRoute.route('/update/:id').post(function (req, res) {
  Collage.findById(req.params.id, function(err, collage) {
    if (!collage)
      res.status(404).send("Record not found");
    else {
      collage.Index_value = req.body.Index_value;
      collage.Name = req.body.Name;
      collage.Start_age = req.body.Start_age;
      collage.Stop_age = req.body.Stop_age;
      collage.Years = req.body.Years;
      collage.MidAmount = req.body.MidAmount;
      collage.Amountt = req.body.Amountt;
      collage.FinalAmount = req.body.FinalAmount;
      

      collage.save().then(collage => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
colRoute.route('/delete/:id').get(function (req, res) {
    Collage.findByIdAndRemove({_id: req.params.id}, function(err, product){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = colRoute;