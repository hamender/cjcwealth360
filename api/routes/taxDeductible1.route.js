const express = require('express');
const app = express();
const ded1Route = express.Router();

// Require Product model in our routes module
let Amount = require('../models/taxDeductible1');

// Defined store route
ded1Route.route('/add').post(function (req, res) {
  let amount = new Amount(req.body);
  amount.save()
    .then(amount => {
      res.status(200).json({'Amount': 'Amount has been added successfully'});
    })     
    .catch(err => {
      console.log(err)
    res.status(400).send("unable to save to database");
    });
});



// Defined get data(index or listing) route
ded1Route.route('/').get(function (req, res) {
  Amount.find(function (err, amount){
    if(err){
      console.log(err);
    }
    else {
      res.json(amount);
    }
  });
});

// Defined edit route
ded1Route.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Amount.findById(id, function (err, amount){
      res.json(amount);
  });
});

//  Defined update route
ded1Route.route('/update/:id').post(function (req, res) {
  Amount.findById(req.params.id, function(err, amount) {
    if (!amount)
      res.status(404).send("Record not found");
    else {
      amount.Index_value = req.body.Index_value;
      amount.Name = req.body.Name;
      amount.Start_age = req.body.Start_age;
      amount.Stop_age = req.body.Stop_age;
      amount.Years = req.body.Years;
      amount.MidAmount = req.body.MidAmount;
      amount.Amountt = req.body.Amountt;
      amount.FinalAmount = req.body.FinalAmount;
      

      amount.save().then(amount => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
ded1Route.route('/delete/:id').get(function (req, res) {
    Amount.findByIdAndRemove({_id: req.params.id}, function(err, product){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = ded1Route;