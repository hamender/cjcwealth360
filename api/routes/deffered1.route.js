const express = require('express');
const app = express();
const def1Route = express.Router();

// Require Product model in our routes module
let Deffered = require('../models/deffered1');

// Defined store route
def1Route.route('/add').post(function (req, res) {
  let deffered = new Deffered(req.body);
  deffered.save()
    .then(deffered => {
      res.status(200).json({'Deffered': 'Deffered has been added successfully'});
    })     
    .catch(err => {
      console.log(err)
    res.status(400).send("unable to save to database");
    });
});



// Defined get data(index or listing) route
def1Route.route('/').get(function (req, res) {
  Deffered.find(function (err, deffered){
    if(err){
      console.log(err);
    }
    else {
      res.json(deffered);
    }
  });
});

// get by name
def1Route.route('/findOne').post(function (req, res) {
  var Name = req.body.name;
  Deffered.findOne({Name:Name}, function (err, amount){
      res.json(amount);

  });
});

// Defined edit route
def1Route.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Deffered.findById(id, function (err, deffered){
      res.json(deffered);
  });
});

//  Defined update route
def1Route.route('/update/:id').post(function (req, res) {
  Deffered.findById(req.params.id, function(err, deffered) {
    if (!deffered)
      res.status(404).send("Record not found");
    else {
      deffered.Index_value = req.body.Index_value;
      deffered.Name = req.body.Name;
      deffered.Start_age = req.body.Start_age;
      deffered.Stop_age = req.body.Stop_age;
      deffered.Years = req.body.Years;
      deffered.MidAmount = req.body.MidAmount;
      deffered.Amountt = req.body.Amountt;
      deffered.FinalAmount = req.body.FinalAmount;
      deffered.Wd = req.body.Wd;
      deffered.Amount2 = req.body.Amount2;

      deffered.save().then(deffered => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
def1Route.route('/delete/:id').get(function (req, res) {
    Deffered.findByIdAndRemove({_id: req.params.id}, function(err, product){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = def1Route;