const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Product
let TaxDeductible = new Schema({
  Index_value:{
    type: Number
  },
  Name:{
    type: String
  },
  Client_Name:{
    type: String
  },
  Spouse_Name:{
    type: String
  },
  Start_age: {
   type: String
  },
  Stop_age: {
      type: String
  },
  Years: {
    type: String
  },
  MidAmount: {
    type: String
  },
  Amountt: {
    type: String
  },
  FinalAmount: {
    type: String
  },
  Wd: {
    type: String
  },
  Amount1: {
    type: String
  }
},{
    collection: 'TaxDeduct1'
});

module.exports = mongoose.model('TaxDeductible', TaxDeductible);