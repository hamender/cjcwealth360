const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Product
let Percent1 = new Schema({
  Index_value:{
    type: Number
  },
  Name:{
    type: String
  },
  Start_age: {
   type: String
  },
  Stop_age: {
      type: String
  },
  percentage: {
    type: String
  },
  Amount: {
    type: String
  },
  Total_Amount: {
    type: String
  },
},{
    collection: 'Deffered_Percent1'
});

module.exports = mongoose.model('Percent1', Percent1);